package cn.me.archko.abslibrary.view;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import cn.me.archko.abslibrary.R;

/**
 * @author: archko 14/11/18 :14:07
 */
public class AKLoadingView extends AKView {

    /**
     * 等待时显示的文本
     */
    protected TextView mWaitingTxt;

    @Override
    public View initView(View view, LayoutInflater inflater) {
        mContainer=view.findViewById(R.id.lay_waiting_container);
        mWaitingTxt=(TextView) view.findViewById(R.id.txt_loading);
        return mContainer;
    }

    /**
     * 显示等待页面,并设置文本信息与其它两个是互斥的.
     *
     * @param resId 资源id
     */
    public void showWaitingContainer(int resId) {
        super.showView();
        if (null!=mWaitingTxt&&resId!=0) {
            mWaitingTxt.setText(resId);
        }
    }
}
