package cn.me.archko.abslibrary.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import cn.me.archko.abslibrary.listener.AKViewListener;

/**
 * @author: archko 14/11/18 :14:13
 */
public abstract class AKView implements AKViewListener {

    Context mContext;
    /**
     * 数据显示区,获取数据结束后,如果数据没有得到,旧数据也为空,则隐藏.
     */
    protected View mContainer;
    Animation mShowAnimation;
    Animation mHideAnimation;

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext=mContext;
    }

    public Animation getShowAnimation() {
        return mShowAnimation;
    }

    public void setShowAnimation(Animation mShowAnimation) {
        this.mShowAnimation=mShowAnimation;
    }

    public Animation getHideAnimation() {
        return mHideAnimation;
    }

    public void setHideAnimation(Animation mHideAnimation) {
        this.mHideAnimation=mHideAnimation;
    }

    @Override
    public View initView(View view, LayoutInflater inflater) {
        return null;
    }

    @Override
    public View getContentView() {
        return mContainer;
    }

    public void setContentView(View view) {
        this.mContainer=view;
    }

    @Override
    public void showView() {
        if (null!=mContainer) {
            if (null!=mShowAnimation) {
                mContainer.setAnimation(mShowAnimation);
            }
            mContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideView() {
        if (null!=mContainer) {
            if (null!=mHideAnimation) {
                mContainer.setAnimation(mHideAnimation);
            }
            mContainer.setVisibility(View.GONE);
        }
    }
}
