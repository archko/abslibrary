package cn.me.archko.abslibrary.view;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import cn.me.archko.abslibrary.R;

/**
 * @author: archko 14/11/18 :14:07
 */
public class AKEmptyView extends AKView {

    protected TextView mNoDataTxt;

    @Override
    public View initView(View view, LayoutInflater inflater) {
        mContainer=view.findViewById(R.id.nodata_container);
        mNoDataTxt=(TextView) view.findViewById(R.id.nodata_txt);
        return mContainer;
    }

    /**
     * 显示空数据区,与其它两个是互斥的.
     *
     * @param resId 空数据的文本
     */
    public void showNoDataContainer(int resId) {
        super.showView();
        if (null!=mNoDataTxt&&resId!=0) {
            mNoDataTxt.setText(resId);
        }
    }

    public void showNoDataContainer(String resId) {
        super.showView();
        if (null!=mNoDataTxt&&resId!=null) {
            mNoDataTxt.setText(resId);
        }
    }
}
