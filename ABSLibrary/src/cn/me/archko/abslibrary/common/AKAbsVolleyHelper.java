package cn.me.archko.abslibrary.common;

import android.app.Dialog;
import android.content.Context;
import cn.me.archko.abslibrary.AKAbsApp;
import cn.me.archko.abslibrary.listener.AKDataListener;
import cn.me.archko.abslibrary.listener.AKListener;
import cn.me.archko.abslibrary.utils.LLog;
import cn.me.archko.abslibrary.volley.AKRequest;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * 辅助类的父类,
 *
 * @author: archko 2014/10/21 :20:47
 */
public class AKAbsVolleyHelper implements AKListener {

    public static final String TAG="AKAbsVolleyHelper";

    private AKBaseDialog mAKDialog=new AKBaseDialog();

    public void setAKDialog(AKBaseDialog baseDialog) {
        this.mAKDialog=baseDialog;
    }

    public Dialog getDialog() {
        return mAKDialog.getDialog();
    }

    public void setDialog(Dialog mDialog) {
        this.mAKDialog.setDialog(mDialog);
    }

    public void showDialog(Context context) {
        showDialog(context, true);
    }

    public void showDialog(Context context, boolean cancelable) {
        mAKDialog.showDialog(context, cancelable);
    }

    public void dismissDialog() {
        mAKDialog.dismissDialog();
    }

    //-------------------=======================================
    //请求方法.子类覆盖,可以用同一个请求返回不同的解析
    //-------------------=======================================

    public void sendRequest(Request request) {
        AKAbsApp.getApp().getVolleyQueue().add(request);
    }

    /**
     * 发送一个请求,能用的,需要覆盖解析,默认使用post方法.
     *
     * @param dataListener 数据监听
     * @param listener     返回数据监听
     * @param url          请求的url
     * @param cls          默认不处理数据
     * @param tag          请求标记
     * @param method       请求方法,目前只有post
     * @param body         请求数据
     */
    public void sendRequest(String url, Object tag, String body, final AKDataListener dataListener,
        Response.Listener listener) {
        sendRequest(url, tag, Request.Method.POST, body, dataListener, listener);
    }

    /**
     * 发送一个请求,能用的,需要覆盖解析
     *
     * @param dataListener 数据监听
     * @param listener     返回数据监听
     * @param url          请求的url
     * @param cls          默认不处理数据
     * @param tag          请求标记
     * @param method       请求方法,目前只有post
     * @param body         请求数据
     */
    public void sendRequest(String url, final Object tag, int method, String body,
        final AKDataListener dataListener, Response.Listener listener) {
        sendRequest(url, tag, method, null, body, dataListener, listener);
    }

    public void sendRequest(String url, final Object tag, int method, Map<String, String> params, String body,
        final AKDataListener dataListener, Response.Listener listener) {
        LLog.d(TAG, "sendRequest:"+url+" body:"+body);
        final AKRequest mGsonObjRequest=new AKRequest(method, url, null, body, listener,
            new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    LLog.d(TAG, "error:"+error);
                    doErrorResponse(error, dataListener, tag);
                }
            }) {
            @Override
            public RetryPolicy getRetryPolicy() {
                RetryPolicy retryPolicy=new DefaultRetryPolicy(9000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                return retryPolicy;
            }

            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                if (null==response||null==response.data) {
                    return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));
                }
                return AKAbsVolleyHelper.this.doParseNetworkResponse(response, tag, mJavaClass);
            }
        };
        mGsonObjRequest.setTag(tag);
        //mGsonObjRequest.addHeader("User-Agent", JSConstants.JS_USER_AGENT);
        mGsonObjRequest.setContentType("application/x-www-form-urlencoded");
        if (null!=params) {
            mGsonObjRequest.setParams(params);
        }

        AKAbsApp.getApp().getVolleyQueue().add(mGsonObjRequest);
    }

    public void doErrorResponse(VolleyError error, AKDataListener dataListener, Object tag) {
        dismissDialog();
        if (null!=dataListener) {
            dataListener.onFailed(error);
        }
    }

    public Response doParseNetworkResponse(NetworkResponse response, Object tag, Class mJavaClass) {
        try {
            String jsonString=new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            LLog.printResult("parseNetworkResponse:"+jsonString);
            Gson mGson=new Gson();
            return Response.success(mGson.fromJson(jsonString, mJavaClass), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        }
    }
}
