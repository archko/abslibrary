package cn.me.archko.abslibrary.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import cn.me.archko.abslibrary.listener.AKViewListener;

/**
 * @author: archko 15/1/12 :15:35
 */
public class AKViewController<T> {

    protected T data;
    /**
     * 数据处理类.子类可以考虑使用抽象工厂和桥模式加载.
     */
    protected AKAbsVolleyHelper mAbsHelper;
    /**
     * 在等待获取数据的视图
     */
    protected AKViewListener mWaitingContainer;
    /**
     * 没有数据时显示的视图,可能为空,如果子类没有初始化就为空.
     */
    protected AKViewListener mNoDataContainer;
    /**
     * 数据显示区,获取数据结束后,如果数据没有得到,旧数据也为空,则隐藏.
     */
    protected AKViewListener mDataContainer;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data=data;
    }

    public AKAbsVolleyHelper getAbsHelper() {
        return mAbsHelper;
    }

    public void setAbsHelper(AKAbsVolleyHelper mAbsHelper) {
        this.mAbsHelper=mAbsHelper;
    }

    public AKViewListener getWaitingContainer() {
        return mWaitingContainer;
    }

    public void setWaitingContainer(AKViewListener mWaitingContainer) {
        this.mWaitingContainer=mWaitingContainer;
    }

    public AKViewListener getNoDataContainer() {
        return mNoDataContainer;
    }

    public void setNoDataContainer(AKViewListener mNoDataContainer) {
        this.mNoDataContainer=mNoDataContainer;
    }

    public AKViewListener getDataContainer() {
        return mDataContainer;
    }

    public void setDataContainer(AKViewListener mDataContainer) {
        this.mDataContainer=mDataContainer;
    }

    //----------------------------------------

    public void initContainer(View view, LayoutInflater inflater, Bundle savedInstanceState) {
    }

    public void loadData() {

    }

    /**
     * 隐藏等待页面
     */
    public void hideWaitingContainer() {
        if (null!=mWaitingContainer) {
            mWaitingContainer.hideView();
        }
    }

    /**
     * 显示等待页面,并设置文本信息与其它两个是互斥的.
     *
     * @param resId 资源id
     */
    public void showWaitingContainer(int resId) {
        if (null!=mWaitingContainer) {
            mWaitingContainer.showView();
        }

        if (null!=mNoDataContainer) {
            mNoDataContainer.hideView();
        }
        if (null!=mDataContainer) {
            mDataContainer.hideView();
        }
    }

    /**
     * 显示空数据区,与其它两个是互斥的.
     *
     * @param resId 空数据的文本
     */
    public void showNoDataContainer(int resId) {
        if (null!=mNoDataContainer) {
            mNoDataContainer.showView();
        }

        if (null!=mDataContainer) {
            mDataContainer.hideView();
        }
        hideWaitingContainer();
    }

    public void showNoDataContainer(String resId) {
        if (null!=mNoDataContainer) {
            mNoDataContainer.showView();
        }

        if (null!=mDataContainer) {
            mDataContainer.hideView();
        }
        hideWaitingContainer();
    }

    /**
     * 显示数据内容.与其它两个是互斥的.
     */
    public void showDataContainer() {
        if (null!=mDataContainer) {
            mDataContainer.showView();
        }

        if (null!=mNoDataContainer) {
            mNoDataContainer.hideView();
        }
        hideWaitingContainer();
    }
}
