package cn.me.archko.abslibrary.common;

import android.widget.BaseAdapter;

/**
 * @author: archko 15/1/12 :15:35
 */
public class AKListViewController<T> extends AKViewController {

    /**
     * 默认一页数据大小
     */
    public static final int DEFAULT_PAGE_SIZE=20;
    /**
     * 当前分页的索引,页码
     */
    protected int currentPage=0;
    /**
     * 下一页页码.要大于mCurr
     */
    protected int targetPage=0;
    protected int pageSize=DEFAULT_PAGE_SIZE;

    protected BaseAdapter mAdapter;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage=currentPage;
    }

    public int getTargetPage() {
        return targetPage;
    }

    public void setTargetPage(int targetPage) {
        this.targetPage=targetPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public BaseAdapter getAdapter() {
        return mAdapter;
    }

    public void setAdapter(BaseAdapter mAdapter) {
        this.mAdapter=mAdapter;
    }

    //----------------------------------------

    /**
     * 隐藏等待页面
     */
    public void hideWaitingContainer() {
        if (null!=mWaitingContainer) {
            mWaitingContainer.hideView();
        }
    }

    /**
     * 显示等待页面,并设置文本信息与其它两个是互斥的.
     *
     * @param resId 资源id
     */
    public void showWaitingContainer(int resId) {
        if (null!=mWaitingContainer) {
            mWaitingContainer.showView();
        }

        if (null!=mNoDataContainer) {
            mNoDataContainer.hideView();
        }
        if (null!=mDataContainer) {
            mDataContainer.hideView();
        }
    }

    /**
     * 显示空数据区,与其它两个是互斥的.
     *
     * @param resId 空数据的文本
     */
    public void showNoDataContainer(int resId) {
        if (null!=mNoDataContainer) {
            mNoDataContainer.showView();
        }

        if (null!=mDataContainer) {
            mDataContainer.hideView();
        }
        hideWaitingContainer();
    }

    public void showNoDataContainer(String resId) {
        if (null!=mNoDataContainer) {
            mNoDataContainer.showView();
        }

        if (null!=mDataContainer) {
            mDataContainer.hideView();
        }
        hideWaitingContainer();
    }

    /**
     * 显示数据内容.与其它两个是互斥的.
     */
    public void showDataContainer() {
        super.showDataContainer();

        if (null!=mAdapter) {
            mAdapter.notifyDataSetChanged();
        }
    }
}
