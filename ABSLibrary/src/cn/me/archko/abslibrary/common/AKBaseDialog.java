package cn.me.archko.abslibrary.common;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import cn.me.archko.abslibrary.listener.AKDialogListener;

/**
 * 对话框帮助类
 *
 * @author: archko 14/11/9 :20:55
 */
public class AKBaseDialog implements AKDialogListener {

    private Dialog mDialog;
    private int theme=0;

    public void setTheme(int theme) {
        this.theme=theme;
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public void setDialog(Dialog dialog) {
        this.mDialog=dialog;
    }

    public void showDialog(Context context) {
        showDialog(context, true);
    }

    public void showDialog(Context context, boolean cancelable) {
        if (null==mDialog) {
            mDialog=new ProgressDialog(context);
        }
        mDialog.setCancelable(cancelable);
        mDialog.show();
    }

    public void dismissDialog() {
        if (null!=mDialog&&mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}