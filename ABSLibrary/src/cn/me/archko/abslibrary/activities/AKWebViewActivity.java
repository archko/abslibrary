package cn.me.archko.abslibrary.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import cn.me.archko.abslibrary.R;
import cn.me.archko.abslibrary.utils.AKNotifyUtil;

/**
 * @author archko
 */
public class AKWebViewActivity extends AKActivity {

    public static final String EXTRA_TITLE="title";
    public static final String EXTRA_URL="url";
    public static final String PRESSED_BACKICON_IMMEDIATE_BACK="PRESSED_BACKICON_IMMEDIATE_BACK";

    private ProgressBar mHorizontalProgress;
    private WebView mWebView;
    private boolean mIsImmediateBack=false;
    private String url;

    private Handler refreshProgressHandler=new Handler() {
        public void handleMessage(Message msg) {
            if (msg.arg1>=100) {
                if (mHorizontalProgress!=null) {
                    mHorizontalProgress.setVisibility(View.GONE);
                }
            } else {
                if (mHorizontalProgress!=null&&msg.arg1>=0) {
                    mHorizontalProgress.setVisibility(View.VISIBLE);
                    mHorizontalProgress.setProgress(msg.arg1);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.ak_web_view);

        String title=getIntent().getStringExtra(EXTRA_TITLE);
        mIsImmediateBack=getIntent().getBooleanExtra(PRESSED_BACKICON_IMMEDIATE_BACK, false);
        mHorizontalProgress=(ProgressBar) findViewById(R.id.progress_horizontal);
        mWebView=(WebView) findViewById(R.id.webview);

        // 设置支持JavaScript
        WebSettings webSettings=mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(true);
        webSettings.setDomStorageEnabled(true);

        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        url=getIntent().getStringExtra(EXTRA_URL);

        mWebView.setWebViewClient(new WebViewClient() {
            //			@Override
//			public boolean shouldOverrideUrlLoading(WebView view, String url) {
//				/*view.loadUrl(url);
//				return true;*/
//				return false; // 返回false 仍然由该 WebView 处理 url
//			}
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onFormResubmission(WebView view, Message dontResend, Message resend) {
                resend.sendToTarget();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                AKNotifyUtil.showToast(getBaseContext(), "网络异常！");
            }

        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (refreshProgressHandler!=null) {
                    if (refreshProgressHandler.hasMessages(0)) {
                        refreshProgressHandler.removeMessages(0);
                    }
                    Message mMessage=refreshProgressHandler.obtainMessage(0, newProgress, 0, null);
                    refreshProgressHandler.sendMessageDelayed(mMessage, 100);
                }

            }
        });

        mWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Uri uri=Uri.parse(url);
                Intent intent=new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        mWebView.loadUrl(url);
        super.onResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK) {
            if (mIsImmediateBack) {
                onBackPressed();
            } else {
                if (mWebView.canGoBack()) {
                    mWebView.goBack();
                } else {
                    onBackPressed();
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}