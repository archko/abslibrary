package cn.me.archko.abslibrary.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import cn.me.archko.abslibrary.R;

/**
 * @author archko 2014-9-21
 */
public class AKActivity extends FragmentActivity {

    public static final int DIALOG_ID_PROGRESS=671;

    private String mDialogTitle; // 窗口的标题
    private String mDialogMessage; // 窗口的提示文本
    private OnCancelListener mOnCancelListener;//窗口取消后回调的listener 目前只用于mCustomDialog和mProgressDialog by wuqiang

    private ProgressDialog mProgressDialog;

    private boolean mCancelable=false; // 对话框是否可取消

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.anim_enter_right, R.anim.anim_leave_left);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.anim_enter_left, R.anim.anim_leave_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * 显示正在加载窗口
     *
     * @param message loading 窗口的提示文本
     */
    public Dialog showProgressDialog(String message) {
        return showProgressDialog(message, true);
    }

    /**
     * 显示正在加载窗口
     *
     * @param message loading 窗口的提示文本
     */
    public Dialog showProgressDialog(String message, boolean cancelable) {
        if (isFinishing()) {
            return null;
        }
        reset();
        mCancelable=cancelable;
        mDialogMessage=message;
        showDialog(DIALOG_ID_PROGRESS);
        return mProgressDialog;
    }

    public Dialog showProgressDialog(boolean cancelable) {
        return showProgressDialog(null, cancelable);
    }

    /**
     * 显示正在加载窗口
     *
     * @param message loading 窗口的提示文本
     */
    public Dialog showProgressDialog(String message, boolean cancelable, OnCancelListener onCancelListener) {
        if (isFinishing()) {
            return null;
        }
        reset();
        mCancelable=cancelable;
        mDialogMessage=message;
        mOnCancelListener=onCancelListener;
        showDialog(DIALOG_ID_PROGRESS);
        return mProgressDialog;
    }

    /**
     * 派生类必须自己保证窗口能正确关闭！例如在设备屏幕方向发生变化后
     */
    public void dismissProgressDialog() {
        Dialog dialog=mProgressDialog;
        if (dialog!=null/* && dialog.isShowing()*/) {
            // dialog.dismiss();
            removeDialog(DIALOG_ID_PROGRESS);
        }
        reset();
    }

    @Override
    protected Dialog onCreateDialog(final int id) {
        switch (id) {
            case DIALOG_ID_PROGRESS: {
                mProgressDialog=new ProgressDialog(this);
                mProgressDialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (mOnCancelListener!=null) {
                            mOnCancelListener.onCancel(dialog);
                        }
                    }
                });
                if (!TextUtils.isEmpty(mDialogMessage)) {
                    mProgressDialog.setTitle(mDialogMessage);
                }
                mProgressDialog.setCancelable(mCancelable);
                //((ProgressDialog)mProgressDialog).setIndeterminateDrawable(getResources().getDrawable(R.drawable.sl_progress_dialog));
                return mProgressDialog;
            }
            default:
                break;
        }

        return super.onCreateDialog(id);
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case DIALOG_ID_PROGRESS:
                if (!TextUtils.isEmpty(mDialogMessage)) {
                    mProgressDialog.setTitle(mDialogMessage);
                }
                mProgressDialog.setCancelable(mCancelable);
                break;
        }
    }

    private void reset() {
        mCancelable=true;
        mDialogTitle="任务";
        mDialogMessage=null;
    }
}
