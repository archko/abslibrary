package cn.me.archko.abslibrary.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import cn.me.archko.abslibrary.fragment.AbsBaseFragment;
import cn.me.archko.abslibrary.utils.AKNotifyUtil;
import cn.me.archko.abslibrary.utils.AKUtil;
import cn.me.archko.abslibrary.utils.LLog;

/**
 * 承载Fragment的公用的Activity,从Intent中传来标题,初始化的类.
 *
 * @author: archko 13-12-16 :下午2:01
 */
public class AKFragmentActivity extends AKActivity {

    public static final String TAG="AKFragmentActivity";
    protected ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "SLFragmentActivity.onCreate");

        Intent intent=getIntent();

        if (null==intent) {
            AKNotifyUtil.showToast(getApplicationContext(),"系统错误.");
            finish();
            return;
        }

        if (AKUtil.hasHoneycomb()&&null!=getActionBar()) {
            mActionBar=getActionBar();
            mActionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
            mActionBar.setDisplayShowTitleEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            //mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }

        String title=intent.getStringExtra("title");
        String className=intent.getStringExtra("fragment_class");
        Bundle bundle=intent.getExtras();
        if (null==bundle) {
            bundle=new Bundle();
        }
        if (!TextUtils.isEmpty(title)) {
            bundle.putString("title", title);
        }
        initFragment(className, bundle);
    }

    public void initFragment(String className, Bundle bundle) {
        Intent intent=getIntent();
        if (null==intent) {
            finish();
            return;
        }

        try {
            Fragment old=getSupportFragmentManager().findFragmentById(android.R.id.content);
            LLog.d("", "initFragment."+className+" old:"+old);
            if (null==old) {
                Fragment newFragment=Fragment.instantiate(this, className, intent.getExtras());
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                ft.add(android.R.id.content, newFragment).commitAllowingStateLoss();
            } else {
                Fragment newFragment=Fragment.instantiate(this, className);
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                ft.add(android.R.id.content, newFragment).commitAllowingStateLoss();
                //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                //ft.addToBackStack(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addFragmentToStack(Fragment newFragment) {
        // Add the fragment to the activity, pushing this transaction
        // on to the back stack.
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        ft.add(android.R.id.content, newFragment);  //add需要背景,否则前面的Fragment不销毁,后面的背景透明的.replace则前面的销毁.
        //replace时,返回,前面SLSelectCategoryFragment标题左侧返回按钮没有了.它使用的是GJLifeActivity的标题.
        //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void popFragment() {
        FragmentManager fm=getSupportFragmentManager();
        if (fm.getBackStackEntryCount()>0) {
            fm.popBackStack(fm.getBackStackEntryAt(0).getId(),
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment=getSupportFragmentManager().findFragmentById(android.R.id.content);
        if (null!=fragment&&fragment instanceof AbsBaseFragment) {
            AbsBaseFragment absFragment=(AbsBaseFragment) fragment;
            if (!absFragment.onBackPressed()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
        //gestureDetector = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId=item.getItemId();
        if (itemId==android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
