package cn.me.archko.abslibrary.volley;

import java.io.UnsupportedEncodingException;

import cn.me.archko.abslibrary.utils.LLog;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Custom implementation of Request<T> class which converts the HttpResponse obtained to Java class objects.
 * Uses GSON library, to parse the response obtained.
 * Ref - JsonRequest<T>
 *
 * @author Mani Selvaraj
 */
public class AKGsonRequest<T> extends AKRequest<T> {

    private Gson mGson;

    public AKGsonRequest(int method, String url, Class<T> cls, String requestBody, Listener<T> listener,
        ErrorListener errorListener) {
        super(method, url, cls, requestBody, listener, errorListener);
        mGson=new Gson();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString=new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            LLog.printResult("parseNetworkResponse:"+mJavaClass+" js:"+jsonString);
            T parsedGSON=mGson.fromJson(jsonString, mJavaClass);
            return Response.success(parsedGSON,
                HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        }
    }
}
