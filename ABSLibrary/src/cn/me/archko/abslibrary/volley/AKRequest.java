package cn.me.archko.abslibrary.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Custom implementation of Request<T> class which converts the HttpResponse obtained to Java class objects.
 * Uses GSON library, to parse the response obtained.
 * Ref - JsonRequest<T>
 *
 * @author Mani Selvaraj
 */
abstract public class AKRequest<T> extends Request<T> {

    /**
     * Charset for request.
     */
    public static final String PROTOCOL_CHARSET="utf-8";

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE=
        String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    protected final Listener<T> mListener;

    protected final String mRequestBody;

    protected Class<T> mJavaClass;
    protected String mContentType=PROTOCOL_CONTENT_TYPE;

    public AKRequest(int method, String url, Class<T> cls, String requestBody, Listener<T> listener,
        ErrorListener errorListener) {
        super(method, url, errorListener);
        mJavaClass=cls;
        mListener=listener;
        mRequestBody=requestBody;
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    private Map<String, String> headers=new HashMap<String, String>();
    private Map<String, String> params=null;

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers=headers;
    }

    public void addHeader(String key, String val) {
        headers.put(key, val);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }

    public void addParam(String key, String val) {
        if (null==params) {
            params=new HashMap<String, String>();
        }
        params.put(key, val);
    }

    public void setParams(Map<String, String> aParams) {
        params=aParams;
    }

    public void setContentType(String mContentType) {
        this.mContentType=mContentType;
    }

    @Override
    public String getBodyContentType() {
        return mContentType;
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody==null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

}
