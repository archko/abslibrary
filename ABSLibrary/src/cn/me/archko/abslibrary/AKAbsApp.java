package cn.me.archko.abslibrary;

import android.app.Application;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * @author: archko 14-2-17 :下午1:46
 */
public class AKAbsApp extends Application {

    public static AKAbsApp sInstance;

    protected RequestQueue mVolleyQueue;

    public static AKAbsApp getApp() {
        return (AKAbsApp) sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance=this;
        mVolleyQueue=Volley.newRequestQueue(sInstance);
    }

    public RequestQueue getVolleyQueue() {
        if (null==mVolleyQueue) {
            mVolleyQueue=Volley.newRequestQueue(sInstance);
        }
        return mVolleyQueue;
    }
}
