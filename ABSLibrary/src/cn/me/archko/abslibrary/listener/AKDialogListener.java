package cn.me.archko.abslibrary.listener;

import android.app.Dialog;
import android.content.Context;

/**
 * @author: archko 14/11/9 :21:02
 */
public interface AKDialogListener {

    public void setTheme(int theme);

    public Dialog getDialog();

    public void setDialog(Dialog dialog);

    public void showDialog(Context context);

    public void showDialog(Context context, boolean cancelable);

    public void dismissDialog();
}