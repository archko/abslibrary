package cn.me.archko.abslibrary.listener;

/**
 * 数据查询接口.所有的数据查询都可以用这个,因为返回的是多个元素.
 *
 * @author: archko 14/10/28 :17:33
 */
public interface AKDataListener {

    public void onSuccess(Object... args);

    public void onFailed(Object... args);
}
