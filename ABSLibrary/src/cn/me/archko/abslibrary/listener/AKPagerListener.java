package cn.me.archko.abslibrary.listener;

/**
 * @description: 滚动的分页指示器.
 * @author: archko 13-7-25 :下午4:03
 */
public interface AKPagerListener {

    /**
     * 页面选中的
     *
     * @param position 当前的位置,从0开始.
     */
    public void onPageSelected(int position);
}
