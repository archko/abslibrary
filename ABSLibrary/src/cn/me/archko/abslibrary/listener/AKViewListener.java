package cn.me.archko.abslibrary.listener;

import android.view.LayoutInflater;
import android.view.View;

/**
 * 视图接口,返回一视图,
 *
 * @author: archko 14/11/18 :14:10
 */
public interface AKViewListener {

    public View initView(View view, LayoutInflater inflater);

    public View getContentView();

    public void setContentView(View view);

    public void showView();

    public void hideView();
}
