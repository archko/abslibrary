package cn.me.archko.abslibrary.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * 与ViewPager冲突,所以覆盖事件.
 *
 * @author: archko 13-12-30 :下午8:04
 */
public class AKHackScrollView extends ScrollView {

    private boolean canScroll;
    private GestureDetector mGestureDetector;
    OnTouchListener mGestureListener;

    public AKHackScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mGestureDetector=new GestureDetector(new YScrollDetector());
        canScroll=true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ev.getAction()==MotionEvent.ACTION_UP)
            canScroll=true;

        return super.onInterceptTouchEvent(ev)&&mGestureDetector.onTouchEvent(ev);
    }

    class YScrollDetector extends SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (canScroll)
                if (Math.abs(distanceY)>=Math.abs(distanceX*1.4))
                    canScroll=true;
                else
                    canScroll=false;
            return canScroll;
        }
    }
}