package cn.me.archko.abslibrary.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

/**
 * 屏幕工具类
 *
 * @author archko
 */
public class AKDisplayUtil {

    private static String TAG="SLDisplayUtil";

    /**
     * 获取屏幕宽度
     *
     * @param activity
     * @return
     */
    public static int getScreenWidth(Activity activity) {
        DisplayMetrics dm=new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenWidth=dm.widthPixels;
        return screenWidth;
    }

    /**
     * 获取屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        WindowManager wm=(WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int screenWidth=wm.getDefaultDisplay().getWidth();
        return screenWidth;
    }

    /**
     * 获取屏幕高度
     *
     * @param activity
     * @return
     */
    public static int getScreenHeight(Activity activity) {
        DisplayMetrics dm=new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenHeight=dm.heightPixels;
        return screenHeight;
    }

    /**
     * 获取屏幕高度
     *
     * @param activity
     * @return
     */
    public static int getScreenHeight(Context context) {
        WindowManager wm=(WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int screenHeight=wm.getDefaultDisplay().getHeight();
        return screenHeight;
    }

    /**
     * 获取屏幕显示像素密度比例
     *
     * @return 屏幕显示像素密度比例
     */
    public static float getDisplayDensity(Context context) {
        DisplayMetrics displayMetrics=context.getResources().getDisplayMetrics();
        return displayMetrics.density;
    }

    public static int convertPxToDp(Context context, int px) {
        WindowManager wm=(WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display=wm.getDefaultDisplay();
        DisplayMetrics metrics=new DisplayMetrics();
        display.getMetrics(metrics);
        float logicalDensity=metrics.density;
        int dp=Math.round(px/logicalDensity);
        return dp;
    }

    public static int convertDpToPx(Context context, int dp) {
        return Math.round(
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics())
        );
    }
}
