package cn.me.archko.abslibrary.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author: archko 14/11/5 :17:51
 */
public class AKMessageDigest {

    public static String MD5(String data) {
        try {
            MessageDigest md=MessageDigest.getInstance("MD5");
            byte[] bytes=md.digest(data.getBytes());
            return bytesToHexString(bytes);
        } catch (NoSuchAlgorithmException e) {
        }
        return data;
    }

    private static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder=new StringBuilder("");
        if (src==null||src.length<=0) {
            return null;
        }
        for (int i=0; i<src.length; i++) {
            int v=src[i]&0xFF;
            String hv=Integer.toHexString(v);
            if (hv.length()<2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }
}
