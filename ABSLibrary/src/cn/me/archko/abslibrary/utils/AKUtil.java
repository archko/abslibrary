package cn.me.archko.abslibrary.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description:
 * @author: archko 13-12-16 :上午11:13
 */
public class AKUtil {

    private static Pattern digitPattern;
    private static Pattern colonPattern;

    public static Pattern getDigitPattern() {
        if (digitPattern==null) {
            digitPattern=Pattern.compile("[[\\s]0-9]{1,20}", Pattern.MULTILINE);
        }
        return digitPattern;
    }

    /**
     * 匹配冒号
     *
     * @return
     */
    public static Pattern getColonPattern() {
        if (colonPattern==null) {
            colonPattern=Pattern.compile("(.{1,3}):", Pattern.MULTILINE);
        }
        return colonPattern;
    }

    /**
     * 对数字高亮
     *
     * @param context
     * @param spannable
     * @param color
     */
    public static void highlightDigitContent(Context context, Spannable spannable, int color) {
        Pattern pattern=Pattern.compile("[[\\s]0-9]{1,20}", Pattern.MULTILINE);

        Matcher atMatcher=pattern.matcher(spannable);

        while (atMatcher.find()) {
            int start=atMatcher.start();
            int end=atMatcher.end();
            if (end-start==1) {
            } else {
                if (end-start<=1) {
                    return;
                }
            }

            ForegroundColorSpan colorSpan=new ForegroundColorSpan(color);
            spannable.setSpan(colorSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    /**
     * 备注高亮
     *
     * @param context
     * @param spannable
     * @param color
     * @param pattern
     */
    public static void highlightContentRemark(Context context, Spannable spannable, int color, Pattern pattern) {
        Matcher atMatcher=pattern.matcher(spannable);

        while (atMatcher.find()) {
            int start=atMatcher.start();
            int end=atMatcher.end();
            if (end-start==1) {
            } else {
                if (end-start<=1) {
                    return;
                }
            }

            ForegroundColorSpan colorSpan=new ForegroundColorSpan(color);
            spannable.setSpan(colorSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            break;
        }
    }

    public static String hidePhonenumber(String phone) {
        if (!TextUtils.isEmpty(phone)&&phone.length()>4) {
            return phone.substring(0, phone.length()-4)+"****";
        }

        return "";
    }

    /**
     * Used to create shortcuts for an artist, album, or playlist that is then
     * placed on the default launcher homescreen
     *
     * @param displayName The shortcut name
     * @param id          The ID of the artist, album, playlist, or genre
     * @param mimeType    The MIME type of the shortcut
     * @param context     The {@link Context} to use to
     */
    public static void createShortcutIntent(final String displayName, final Activity context) {
        /*try {
            Bitmap bitmap=null;
            if (bitmap==null) {
                bitmap=BitmapFactory.decodeResource(context.getResources(), R.drawable.dial_shortcut);
            }

            // Intent used when the icon is touched
            final Intent shortcutIntent=new Intent(context, ShortcutActivity.class);
            shortcutIntent.setAction(Intent.ACTION_VIEW);
            shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //if (AKUtil.hasHoneycomb()) {
                //shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //}
            shortcutIntent.putExtra(SLHomeTabs.CURRENT_ITEM, 2);

            // Intent that actually sets the shortcut
            final Intent intent=new Intent();
            //intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, BitmapUtils.resizeAndCropCenter(bitmap, 96));
            intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmap);
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, displayName);
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            context.sendBroadcast(intent);
            GUtil.showToast("产生快捷方式了.");
        } catch (final Exception e) {
            DLog.e("", "createShortcutIntent", e);
        }*/
    }

    /**
     * Used to determine if the current device is a Google TV
     *
     * @param context The {@link Context} to use
     * @return True if the device has Google TV, false otherwise
     */
    public static final boolean isGoogleTV(final Context context) {
        return context.getPackageManager().hasSystemFeature("com.google.android.tv");
    }

    /**
     * Used to determine if the device is running Froyo or greater
     *
     * @return True if the device is running Froyo or greater, false otherwise
     */
    public static final boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    /**
     * Used to determine if the device is running Gingerbread or greater
     *
     * @return True if the device is running Gingerbread or greater, false
     *         otherwise
     */
    public static final boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    /**
     * Used to determine if the device is running Honeycomb or greater
     *
     * @return True if the device is running Honeycomb or greater, false
     *         otherwise
     */
    public static final boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * Used to determine if the device is running Honeycomb-MR1 or greater
     *
     * @return True if the device is running Honeycomb-MR1 or greater, false
     *         otherwise
     */
    public static final boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    /**
     * Used to determine if the device is running ICS or greater
     *
     * @return True if the device is running Ice Cream Sandwich or greater,
     *         false otherwise
     */
    public static final boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    /**
     * Used to determine if the device is running Jelly Bean or greater
     *
     * @return True if the device is running Jelly Bean or greater, false
     *         otherwise
     */
    public static final boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    /**
     * Used to determine if the device is a tablet or not
     *
     * @param context The {@link Context} to use.
     * @return True if the device is a tablet, false otherwise.
     */
    public static final boolean isTablet(final Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Used to determine if the device is currently in landscape mode
     *
     * @param context The {@link Context} to use.
     * @return True if the device is in landscape mode, false otherwise.
     */
    public static final boolean isLandscape(final Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    /**
     * Execute an {@link android.os.AsyncTask} on a thread pool
     *
     * @param forceSerial True to force the task to run in serial order
     * @param task Task to execute
     * @param args Optional arguments to pass to
     *            {@link android.os.AsyncTask#execute(Object[])}
     * @param <T> Task argument type
     */
    @SuppressLint("NewApi")
    public static <T> void execute(final boolean forceSerial, final AsyncTask<T, ?, ?> task,
        final T... args) {
        final WeakReference<AsyncTask<T, ?, ?>> taskReference = new WeakReference<AsyncTask<T, ?, ?>>(
            task);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.DONUT) {
            throw new UnsupportedOperationException(
                "This class can only be used on API 4 and newer.");
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB || forceSerial) {
            taskReference.get().execute(args);
        } else {
            taskReference.get().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, args);
        }
    }

    /**
     * Execute an {@link AsyncTask} on a thread pool
     *
     * @param forceSerial True to force the task to run in serial order
     * @param task Task to execute
     * @param args Optional arguments to pass to
     *            {@link AsyncTask#execute(Object[])}
     * @param <T> Task argument type
     */
    /*@SuppressLint("NewApi")
    public static <T> void executeCustomTask(final boolean forceSerial, final cn.archko.thread.AsyncTask<T, ?, ?> task,
        final T... args) {
        final WeakReference<cn.archko.thread.AsyncTask<T, ?, ?>> taskReference = new WeakReference<cn.archko.thread.AsyncTask<T, ?, ?>>(
            task);
        taskReference.get().executeOnExecutor(cn.archko.thread.AsyncTask.THREAD_POOL_EXECUTOR, args);
    }*/
}
