package cn.me.archko.abslibrary.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * 工具类，文件相关操作
 *
 * @author ZhangYunfang
 */
public final class AKFileUtil {

    /**
     * 存储路径方式
     */
    public enum PathType {
        /**
         * 存储在SDCard中
         */
        SDCARD,
        /**
         * 存储在软件Cache中
         */
        CACHE,
        /**
         * 存储在软件DATA目录下的app_photos中
         */
        APP_PHOTOS,
        /**
         * 存储在多媒体文件夹下
         */
        SDCARD_PHOTOS,
        /**
         * 存储在多媒体文件夹下
         */
        MEDIA_DIR,
        /**
         * 存储在软件DATA目录下的app_xmls中
         */
        APP_XML
    }

    /**
     * 客户端发帖最大照片文件大小，30kb，如果大于，就需要压缩
     */
    public static final int MAX_POST_PHOTO_FILE_SIZE=30720;

    /**
     * 图片最大边像素
     */
    public static final int MAX_POST_PHOTO_PX=600;

    /**
     * 图片质量
     */
    public static final int PHOTO_QUANLITY_CAMER=80;
    public static final int PHOTO_QUANLITY_META=50;
    public static final int PHOTO_QUANLITY_SMALLMETA=70;
    /**
     * 图片格式
     */
    public static final String PHOTO_FORMATE="jpg";

    /**
     * 赶集 照片子路径
     */
    public static final String PHOTO_DIR="photos";

    /**
     * XML文件路径
     */
    public static final String XML_DIR="xmls";

    /**
     * Image jpg文件后缀
     */
    public static final String EXE_JPG="jpg";

    /**
     * 获得文件的路径，如果有SD卡，那么首先获得SD卡的路径
     */
    public static String getPathSDCardFirst(Context context, String dir) {
        String absolutePath="";
        if (sdcardAvailable()) {
            absolutePath=createSavePath(context, PathType.SDCARD);
            absolutePath+=File.separator+dir;
        } else {
            absolutePath=context.getDir(dir, Context.MODE_PRIVATE).getPath();
        }
        return absolutePath;
    }

    /**
     * 判断是否有SD卡
     */
    public static boolean sdcardAvailable() {
        String status=Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getDefaultSavePath(Context context) {

        PathType pathType;
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            pathType=PathType.CACHE;
        } else {
            pathType=PathType.SDCARD;
        }
        return AKFileUtil.createSavePath(context.getApplicationContext(), pathType);
    }

    /**
     * 获得文件的路径
     */
    public static String createSavePath(Context context, PathType pathType) {
        String path;
        switch (pathType) {
            case CACHE:
                path=context.getCacheDir().getPath();
                break;
            case APP_PHOTOS:
                path=context.getDir(PHOTO_DIR, Context.MODE_WORLD_WRITEABLE).getPath();
                break;
            case APP_XML:
                path=context.getDir(XML_DIR, Context.MODE_WORLD_WRITEABLE).getPath();
                break;
            case SDCARD:
                path=Environment.getExternalStorageDirectory().getPath();
                break;
            case SDCARD_PHOTOS:
                path=Environment.getExternalStorageDirectory().getPath()+"/"+PHOTO_DIR;
                File fileDir=new File(path);
                if (!fileDir.exists()) {
                    fileDir.mkdir();
                }
                break;
            default:
                path=Environment.getExternalStorageDirectory().getPath();
                break;
        }
        return path;
    }

    /**
     * 创建指定路径的文件夹
     */
    public static File createDir(String path) throws SecurityException {
        File fileDir=new File(path);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        return fileDir;
    }

    /**
     * 创建文件
     */
    public static boolean createFile(File file) throws IOException {
        if (file.exists()) {
            deleteFile(file);
        }
        File parent=file.getParentFile();
        if (parent!=null&&!parent.exists()) {
            parent.mkdirs();
        }
        return file.createNewFile();
    }

    /**
     * 文件Uri来自于Media Provider
     */
    public static final int FROM_TYPE_MEDIA=1;

    /**
     * 文件Uri来自于存储设备或是指定的存储路径
     */
    public static final int FROM_TYPE_STORE=2;

    /**
     * 从文件Uri获取该文件的容量大小
     */
    public static long getFileSize(Context context, Uri fileUri, int fromType) {
        long fileSizeinByte=0;
        String mediaFilePath=null;
        switch (fromType) {
            case FROM_TYPE_MEDIA:
                mediaFilePath=getMediaRealPath(context, fileUri);
                break;
            case FROM_TYPE_STORE:
                mediaFilePath=fileUri.getEncodedPath();
                break;
            default:
                break;
        }
        try {
            if (mediaFilePath!=null) {
                File mediaFile=new File(mediaFilePath);
                fileSizeinByte=mediaFile.length();
            }
        } catch (Exception e) {
        }
        return fileSizeinByte;
    }

    /**
     * 获取媒体文件真实文件路径
     */
    public static String getMediaRealPath(Context context, Uri contentUri) {
        String[] projection={MediaStore.Images.Media.DATA};
        Cursor cursor=context.getContentResolver().query(contentUri, projection, // Which columns to return
            null, // WHERE clause; which rows to return (all rows)
            null, // WHERE clause selection arguments (none)
            null); // Order-by clause (ascending by name)
        int column_index=cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * 将源文件拷贝到目标文件
     */
    public static boolean copyFile(File src, File dst) {
        try {
            InputStream in=new FileInputStream(src);
            if (!dst.exists()) {
                dst.createNewFile();
            }
            OutputStream out=new FileOutputStream(dst);
            //StreamUtil.copyStream(in, out);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 从文件的Uri(带有file://开头的Uri)删除该文件
     */
    public static final boolean deleteFile(Uri fileUri) {
        File tempFile=new File(fileUri.getEncodedPath());
        return deleteFile(tempFile);
    }

    /**
     * 删除指定文件
     */
    public static final boolean deleteFile(File file) {
        boolean result=true;
        if (file.isDirectory()) {
            File[] children=file.listFiles();
            if (children!=null) {
                for (int i=0; i<children.length; i++) {
                    result&=deleteFile(children[i]);
                }
            }
        }
        result&=file.delete();
        return result;
    }

    /**
     * 根据文件的最后修改时间来删除旧的文件，解决可能出现空指针的问题
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void deleOldFile(String dirPtah, int fileNums) throws Exception {
        File file=new File(dirPtah);
        File[] listFiles=file.listFiles();
        if (listFiles==null||listFiles.length<1) {
            return;
        }
        ArrayList<File> list=new ArrayList<File>();
        for (File file0 : listFiles) {
            list.add(file0);
        }

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                if (((File) o1).lastModified()>((File) o2).lastModified()) {
                    return 1;
                } else if (((File) o1).lastModified()==((File) o2).lastModified()) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        while (list.size()>fileNums) {
            File f=list.get(0);
            if (f.exists()) {
                f.delete();
            }
            list.remove(0);
        }
    }

    /**
     * 删除2天之前的文件，如果文件夹为空，则删除之
     */
    public static void deleteOldFile(File dir) {
        deleteOldFile(dir, 0);
    }

    private static void deleteOldFile(File dir, int depth) {
        try {
            long now=System.currentTimeMillis();
            long expire=2*24*3600*1000; // 2天
            for (File f : dir.listFiles()) {
                if (f.isDirectory()&&depth<10) {
                    deleteOldFile(f, depth+1);
                } else if ((now-f.lastModified())>expire) {
                    f.delete();
                }
            }

            // 如果文件夹已经为空，删除
            if (dir.listFiles().length==0) {
                dir.delete();
            }
        } catch (Throwable e) {
        }
    }

    /**
     * 移除文件
     */
    public static void remmoveFile(String filePathName) throws FileNotFoundException, IOException {
        File f=new File(filePathName);
        if (f.exists()) {
            AKFileUtil.deletE(f);
        }
    }

    /**
     * 删除文件夹或文件下面的文件
     */
    public static void deletE(File f) {
        File[] ff;
        int length;
        if (f.isFile()) {
            f.delete();
            return;
        } else if (f.isDirectory()) {
            ff=f.listFiles();
            length=ff.length;
            int i=0;
            while (length!=0) {
                deletE(ff[i]);
                i++;
                length--;
            }
            if (length==0) {
                f.delete();
                return;
            }
        }
    }

    //-------------------=======================================

    public static String parseInputStream(InputStream is) throws IOException {
        BufferedReader reader=new BufferedReader(new InputStreamReader(is), 1000);
        StringBuilder responseBody=new StringBuilder();
        String line=reader.readLine();
        while (line!=null) {
            responseBody.append(line);
            line=reader.readLine();
        }
        String string=responseBody.toString();
        return string;
    }

    /**
     * 保存字节数组资源,以文件保存,直接写入流
     *
     * @param bytes    流
     * @param filePath 保存的路径,全路径
     * @return 是否保存成功.
     */
    public static boolean saveBytesAsFile(byte[] bytes, String filePath) throws IOException {
        BufferedOutputStream fos=null;
        try {
            fos=new BufferedOutputStream(new FileOutputStream(filePath), 512);

            fos.write(bytes);
            fos.flush();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fos!=null) {
                fos.close();
            }
        }

        return false;
    }

    public static void serializeObject(Object obj, String filename) {
        try {
            File file=new File(filename);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream(file, false));
            out.writeObject(obj);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object deserializeObject(String filename) {
        try {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream(filename));
            Object object=in.readObject();
            in.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object deserializeObject(byte[] data) {
        if (data!=null&&data.length>0) {
            try {
                ObjectInputStream in=new ObjectInputStream(new ByteArrayInputStream(data));
                Object obj=in.readObject();
                in.close();
                return obj;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static byte[] getSerializedBytes(Object obj) {
        try {
            ByteArrayOutputStream bao=new ByteArrayOutputStream();
            ObjectOutputStream out=new ObjectOutputStream(bao);
            out.writeObject(obj);
            out.flush();
            byte[] data=bao.toByteArray();
            out.close();
            bao.close();
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[]{};
    }

    public static Object deepClone(Serializable obj) {
        try {
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            ObjectOutputStream oos=new ObjectOutputStream(baos);
            oos.writeObject(obj);

            ByteArrayInputStream bais=new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois=new ObjectInputStream(bais);
            return ois.readObject();
        } catch (IOException e) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public static String readAsset(Context context, String assetName, String defaultS) {
        try {
            InputStream is=context.getAssets().open(assetName);
            BufferedReader r=new BufferedReader(new InputStreamReader(is, "UTF8"));
            StringBuilder sb=new StringBuilder();
            String line=r.readLine();
            if (line!=null) {
                sb.append(line);
                line=r.readLine();
                while (line!=null) {
                    sb.append('\n');
                    sb.append(line);
                    line=r.readLine();
                }
            }
            return sb.toString();
        } catch (IOException e) {
            return defaultS;
        }
    }

    /**
     * Writes the current app logcat to a file.
     *
     * @param filename The filename to save it as
     * @throws IOException
     */
    public static void writeLogcat(String filename) throws IOException {
        String[] args={"logcat", "-v", "time", "-d"};

        Process process=Runtime.getRuntime().exec(args);
        InputStreamReader input=new InputStreamReader(
            process.getInputStream());
        OutputStreamWriter output=new OutputStreamWriter(
            new FileOutputStream(filename));
        BufferedReader br=new BufferedReader(input);
        BufferedWriter bw=new BufferedWriter(output);
        String line;

        while ((line=br.readLine())!=null) {
            bw.write(line);
            bw.newLine();
        }

        bw.close();
        output.close();
        br.close();
        input.close();
    }
}
