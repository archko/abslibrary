package cn.me.archko.abslibrary.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import cn.me.archko.abslibrary.activities.AKFragmentActivity;

/**
 * 导航类,用于各种的Fragment切换与Activity跳转
 *
 * @author: archko 2014/9/10 :9:39
 */
public class AKNavigationUtil {

    /**
     * 启动新的Activity
     *
     * @param self
     * @param bundle
     * @param cls    目标Activity
     */
    public static void startActivity(Context self, Bundle bundle, Class cls) {
        Intent intent=new Intent(self, cls);
        if (null!=bundle) {
            intent.putExtras(bundle);
        }
        self.startActivity(intent);
    }

    /**
     * 启动一个Activity,带的是Fragment,默认使用AKFragmentActivity作为Activity
     *
     * @param self
     * @param bundle            数据
     * @param fragmentClassName 对应的Fragment
     */
    public static void startFragmentActivity(Context self, Bundle bundle, String fragmentClassName) {
        startFragmentActivity(self, bundle, fragmentClassName, AKFragmentActivity.class);
    }

    /**
     * 启动一个Activity，之后可能去除其它的方法,保留这两个,以传入的参数决定启动对象.可以启动一个Activity,然后承载着Fragment,
     * 也可以没有Fragment,在于传入的参数cls与fragmentClassName.
     *
     * @param self              自身,用于启动的
     * @param bundle            参数,目前都以Bundle作为参数传递媒介
     * @param fragmentClassName 启动的Fragment类名
     * @param cls               承载Fragment的Activity,没有检查类型,调用时需要注意.这里要FragmentActivity
     */
    public static void startFragmentActivity(Context self, Bundle bundle, String fragmentClassName, Class cls) {
        Intent intent=new Intent(self, cls);
        if (null!=bundle) {
            intent.putExtras(bundle);
        }
        intent.putExtra("fragment_class", fragmentClassName);
        self.startActivity(intent);
    }

    /**
     * 由Fragment启动一个Fragment.且有返回值的,它由Activity启动
     *
     * @param activity    请求者
     * @param requestCode 请求的代码
     * @param className   启动的Fragment类名,
     * @param title       标题
     * @param bundle      数据,可为空
     */
    public static void startFragmentForResultByFragment(Context activity, Fragment fragment, int requestCode, String className,
        String title, Bundle bundle) {
        startFragmentForResultByFragment(activity, fragment, requestCode, className, title, bundle, AKFragmentActivity.class);
    }

    /**
     * 由Fragment启动一个Fragment.且有返回值的,它由Activity启动
     *
     * @param activity    请求者
     * @param requestCode 请求的代码
     * @param className   Fragment类名,
     * @param title       标题
     * @param bundle      数据,可为空
     */
    public static void startFragmentForResultByFragment(Context activity, Fragment fragment, int requestCode, String className,
        String title, Bundle bundle, Class clazz) {
        Intent intent=new Intent(activity, clazz);
        intent.putExtra("title", title);
        intent.putExtra("fragment_class", className);
        if (null!=bundle) {
            intent.putExtras(bundle);
        }
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * 开打可返回数据的Activity
     *
     * @param activity    启动者
     * @param bundle
     * @param clazz       目录Activity
     * @param requestCode 请求码
     */
    public static void startActivityForResult(Activity activity, Bundle bundle, Class clazz, int requestCode) {
        Intent intent=new Intent(activity, clazz);
        if (null!=bundle) {
            intent.putExtras(bundle);
        }
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 启动一个带返回值的Fragment
     *
     * @param activity
     * @param requestCode
     * @param className   启动的Fragment
     * @param title
     * @param bundle
     */
    public static void startFragmentForResultByActivity(Activity activity, int requestCode, String className,
        String title, Bundle bundle) {
        startFragmentForResultByActivity(activity, requestCode, className, title, bundle, AKFragmentActivity.class);
    }

    /**
     * 启动一个带返回值的Fragment
     *
     * @param activity
     * @param requestCode
     * @param className   启动的Fragment
     * @param title
     * @param bundle
     * @param clazz       启动的FragmentActivity,用于承载Fragment
     */
    public static void startFragmentForResultByActivity(Activity activity, int requestCode, String className,
        String title, Bundle bundle, Class clazz) {
        Intent intent=new Intent(activity, clazz);
        intent.putExtra("title", title);
        intent.putExtra("fragment_class", className);
        if (null!=bundle) {
            intent.putExtras(bundle);
        }
        activity.startActivityForResult(intent, requestCode);
    }
}
