package cn.me.archko.abslibrary.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.me.archko.abslibrary.common.AKAbsVolleyHelper;
import cn.me.archko.abslibrary.utils.LLog;

/**
 * 基础类,提供加载数据的页面,等待页面与空数据页面的切换,但需椰类初始化布局.
 * 提供基础的Volley方法.
 *
 * @author: archko Date: 14-1-7 Time: 下午4:44
 */
public abstract class AbsBaseFragment<T> extends Fragment {

    public static final String TAG="AbsBaseFragment";
    /**
     * 是否正在加载，暂时无用
     */
    public boolean isLoading=false;
    /**
     * 更新变量,因为有些情况,比如刷新时进入下一页面,再回来,这时如果不更新ui,则会出现异常.
     * 在onResume中查看变量,如果在非ui线程更新了数据,需要设置这个值.然后更新ui.因为线程通常是判断isResumed()
     */
    public boolean isReadyToRefresh=false;
    /**
     * 是否已经更新过,下载过评论数据.
     */
    protected boolean hasUpdated=false;

    //----------------------------------------------
    /**
     * 数据处理类.子类可以考虑使用抽象工厂和桥模式加载.
     */
    protected AKAbsVolleyHelper mVolleyHelper;

    public void setVolleyHelper(AKAbsVolleyHelper volleyHelper) {
        this.mVolleyHelper=volleyHelper;
    }

    protected Handler mHandler=new Handler();

    protected View.OnClickListener mBackListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackClick(v);
        }
    };

    /**
     * 左侧按钮点击事件
     *
     * @param v
     */
    protected void onBackClick(View v) {
    }

    public AbsBaseFragment() {
    }

    /**
     * 由于Fragment是嵌入到Activity,但无法控制后退键,所以在顶级超类添加后退键的方法.
     *
     * @return
     */
    public boolean onBackPressed() {
        return false;
    }

    public void onPageSelected() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LLog.v(TAG, "onCreate:"+this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LLog.v(TAG, "onActivityCreated:"+this);
        isLoading=false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LLog.d(TAG, "onDestroy:"+this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=_onCreateView(inflater, container, savedInstanceState);
        if (null==view) {
            view=super.onCreateView(inflater, container, savedInstanceState);
        }
        initContainer(view, inflater, savedInstanceState);

        return view;
    }

    public View _onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LLog.v(TAG, "onViewCreated:"+this);
    }

    @Override
    public void onStart() {
        super.onStart();
        LLog.v(TAG, "onStart:"+this);
    }

    @Override
    public void onResume() {
        super.onResume();
        LLog.v(TAG, "onResume:"+this);
    }

    @Override
    public void onPause() {
        super.onPause();
        LLog.v(TAG, "onPause:"+this);
    }

    @Override
    public void onStop() {
        super.onStop();
        LLog.v(TAG, "onStop:"+this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        LLog.v(TAG, "onAttach:"+this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LLog.v(TAG, "onDetach:"+this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LLog.v(TAG, "onDestroyView:"+this);
    }

    //----------------------------------------------

    /**
     * 供子类初始化视图,不作抽象方法处理
     */
    public void initContainer(View view, LayoutInflater inflater, Bundle savedInstanceState) {
    }

    //----------------------------------------------
}
