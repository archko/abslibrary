package cn.archko.abs.demo.response;

/**
 * @author: archko 14/11/10 :20:48
 */
public class NewNotify {

    public int dynamic;//":"动态数量， int",
    public String schedule;//":"课程表更新数量， int",
    public String schoolNotify;//":"学校新通知数量， int",
    public String story;//":"新故事数量， int"

    public int getDynamic() {
        return dynamic;
    }

    public void setDynamic(int dynamic) {
        this.dynamic=dynamic;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule=schedule;
    }

    public String getSchoolNotify() {
        return schoolNotify;
    }

    public void setSchoolNotify(String schoolNotify) {
        this.schoolNotify=schoolNotify;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story=story;
    }

    @Override
    public String toString() {
        return "NewNotifyResponse{"+
            "dynamic="+dynamic+
            ", schedule='"+schedule+'\''+
            ", schoolNotify='"+schoolNotify+'\''+
            ", story='"+story+'\''+
            '}';
    }
}
