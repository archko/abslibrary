package cn.archko.abs.demo.response;

/**
 * @author: archko 14/11/10 :20:48
 */
public class DynamicResponseData extends ResponseData {

    public DynamicResponse responseData;

    public DynamicResponse getResponseData() {
        return responseData;
    }

    public void setResponseData(DynamicResponse responseData) {
        this.responseData=responseData;
    }

    @Override
    public String toString() {
        return "LoginResponseData{"+
            "responseData="+responseData+
            '}';
    }
}
