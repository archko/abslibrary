package cn.archko.abs.demo.response;

/**
 * @author: archko 14/11/10 :20:48
 */
public class LoginResponseData extends ResponseData {

    public LoginResponse responseData;

    public LoginResponse getResponseData() {
        return responseData;
    }

    public void setResponseData(LoginResponse responseData) {
        this.responseData=responseData;
    }

    @Override
    public String toString() {
        return "LoginResponseData{"+
            "responseData="+responseData+
            '}';
    }
}
