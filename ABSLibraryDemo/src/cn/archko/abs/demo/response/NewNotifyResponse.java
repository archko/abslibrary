package cn.archko.abs.demo.response;

/**
 * @author: archko 14/11/10 :20:48
 */
public class NewNotifyResponse {

    public NewNotify newNotify;

    public NewNotify getNewNotify() {
        return newNotify;
    }

    public void setNewNotify(NewNotify newNotify) {
        this.newNotify=newNotify;
    }

    @Override
    public String toString() {
        return "NewNotifyResponse{"+
            "newNotify="+newNotify+
            '}';
    }
}
