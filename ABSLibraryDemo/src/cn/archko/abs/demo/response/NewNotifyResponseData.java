package cn.archko.abs.demo.response;

/**
 * @author: archko 14/11/10 :20:48
 */
public class NewNotifyResponseData extends ResponseData {

    public NewNotifyResponse responseData;

    public NewNotifyResponse getResponseData() {
        return responseData;
    }

    public void setResponseData(NewNotifyResponse responseData) {
        this.responseData=responseData;
    }

    @Override
    public String toString() {
        return "NewNotifyResponseData{"+
            "responseData="+responseData+
            '}';
    }
}
