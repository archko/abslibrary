package cn.archko.abs.demo.response;

import cn.archko.abs.demo.model.Dynamic;

/**
 * @author: archko 14/11/9 :21:40
 */
public class DynamicResponse extends ResponseData {

    public Dynamic dynamic;

    public Dynamic getDynamic() {
        return dynamic;
    }

    public void setDynamic(Dynamic dynamic) {
        this.dynamic=dynamic;
    }

    @Override
    public String toString() {
        return "DynamicResponse{"+
            "dynamic="+dynamic+
            '}';
    }
}
