package cn.archko.abs.demo.response;

/**
 * @author: archko 14/11/10 :20:48
 */
public class ListDynamicResponseData extends ResponseData {

    public Page page;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    @Override
    public String toString() {
        return "ListDynamicResponseData{"+
            "page="+page+
            '}';
    }
}
