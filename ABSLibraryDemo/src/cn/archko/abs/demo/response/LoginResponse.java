package cn.archko.abs.demo.response;

import cn.archko.abs.demo.model.User;

/**
 * @author: archko 14/11/9 :21:22
 */
public class LoginResponse extends ResponseData {

    public String sessionID;//会话ID",
    public User gwMember;// {}
    public String succeed;

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID=sessionID;
    }

    public User getGwMember() {
        return gwMember;
    }

    public void setGwMember(User gwMember) {
        this.gwMember=gwMember;
    }

    public String getSucceed() {
        return succeed;
    }

    public void setSucceed(String succeed) {
        this.succeed=succeed;
    }

    @Override
    public String toString() {
        return "LoginResponse{"+
            "sessionID='"+sessionID+'\''+
            ", gwMember="+gwMember+
            ", succeed='"+succeed+'\''+
            '}';
    }
}
