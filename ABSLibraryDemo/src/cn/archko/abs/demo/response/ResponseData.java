package cn.archko.abs.demo.response;

import cn.archko.abs.demo.model.Notification;

/**
 * @author: archko 14/11/9 :21:20
 */
public class ResponseData {

    //ResponseData responseData;

    public Notification notification;//返回的对象.

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification=notification;
    }

    @Override
    public String toString() {
        return "ResponseObject{"+
            ", notification="+notification+
            '}';
    }
}
