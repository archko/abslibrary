package cn.archko.abs.demo.response;

import cn.archko.abs.demo.model.Dynamic;

import java.util.ArrayList;

/**
 * @author: archko 14/11/10 :20:52
 */
public class Page {

    public int total;//": "总数",
    public int index;//": "当前页码",
    public int size;//": "该页内容数",
    public ArrayList<Dynamic> listDynamic;//": []

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total=total;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index=index;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size=size;
    }

    public ArrayList<Dynamic> getListDynamic() {
        return listDynamic;
    }

    public void setListDynamic(ArrayList<Dynamic> listDynamic) {
        this.listDynamic=listDynamic;
    }

    @Override
    public String toString() {
        return "Page{"+
            "total="+total+
            ", index="+index+
            ", size="+size+
            ", listDynamic="+listDynamic+
            '}';
    }
}
