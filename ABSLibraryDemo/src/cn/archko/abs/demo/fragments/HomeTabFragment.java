package cn.archko.abs.demo.fragments;

import android.os.Bundle;
import cn.archko.abs.demo.R;

/**
 * @author: archko 2014/10/16 :9:50
 */
public class HomeTabFragment extends JSSlidingTabsColorsFragment {

    final String[] titles=new String[3];

    @Override
    public void addTab() {
        titles[0]=getString(R.string.tab_title1);
        titles[1]=getString(R.string.tab_title2);
        titles[2]=getString(R.string.tab_title3);

        String title=titles[0];
        Bundle bundle=new Bundle();
        mTabs.add(new SamplePagerItem(HomeFragment.class, bundle, title));

        title=titles[1];
        bundle=new Bundle();
        mTabs.add(new SamplePagerItem(ProfileFragment.class, bundle, title));

        title=titles[2];
        bundle=new Bundle();
        mTabs.add(new SamplePagerItem(ProfileFragment.class, bundle, title));
    }

    public void updateTab(String text, int idx) {
        /*if (null!=indicator&&null!=mTabsAdapter&&mTabsAdapter.getCount()>idx) {
            String txt=String.format(getString(R.string.js_device_tab_count), text);
            txt=titles[idx]+txt;
        }*/
    }
}
