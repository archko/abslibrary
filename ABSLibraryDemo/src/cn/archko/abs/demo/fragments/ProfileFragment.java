package cn.archko.abs.demo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.archko.abs.demo.R;
import cn.me.archko.abslibrary.fragment.AbsBaseFragment;
import cn.me.archko.abslibrary.widget.AKActionBar;

/**
 * @author: archko 2014/10/23 :17:15
 */
public class ProfileFragment extends AbsBaseFragment {

    @Override
    public View _onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.tab_profile, container, false);
        initContainer(root, inflater, savedInstanceState);
        AKActionBar akActionBar=(AKActionBar) root.findViewById(R.id.ak_actionbar);
        akActionBar.setTitle("我的page");
        akActionBar.hideBack();

        return root;
    }
}
