package cn.archko.abs.demo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.archko.abs.demo.BApp;
import cn.archko.abs.demo.R;
import cn.archko.abs.demo.view.SlidingTabLayout;
import cn.me.archko.abslibrary.fragment.AbsBaseFragment;
import cn.me.archko.abslibrary.utils.LLog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * A basic sample which shows how to use {@link com.example.android.common.view.SlidingTabLayout}
 * to display a custom {@link android.support.v4.view.ViewPager} title strip which gives continuous feedback to the user
 * when scrolling.
 */
public class JSSlidingTabsColorsFragment extends AbsBaseFragment {

    /**
     * This class represents a tab to be displayed by {@link android.support.v4.view.ViewPager} and it's associated
     * {@link SlidingTabLayout}.
     */
    public static class SamplePagerItem {

        private final CharSequence mTitle;
        private final int mIndicatorColor;
        private final int mDividerColor;
        private final Class<?> clss;
        private final Bundle args;

        SamplePagerItem(Class<?> _class, Bundle _args, CharSequence title) {
            clss=_class;
            args=_args;
            mTitle=title;
            mIndicatorColor=BApp.getApp().getResources().getColor(R.color.js_tab_indicator_selected_color);
            mDividerColor=BApp.getApp().getResources().getColor(R.color.transparent);
        }

        SamplePagerItem(Class<?> _class, Bundle _args, CharSequence title, int indicatorColor, int dividerColor) {
            clss=_class;
            args=_args;
            mTitle=title;
            mIndicatorColor=indicatorColor;
            mDividerColor=dividerColor;
        }

        /**
         * @return the title which represents this tab. In this sample this is used directly by
         * {@link android.support.v4.view.PagerAdapter#getPageTitle(int)}
         */
        CharSequence getTitle() {
            return mTitle;
        }

        /**
         * @return the color to be used for indicator on the {@link SlidingTabLayout}
         */
        int getIndicatorColor() {
            return mIndicatorColor;
        }

        /**
         * @return the color to be used for right divider on the {@link SlidingTabLayout}
         */
        int getDividerColor() {
            return mDividerColor;
        }
    }

    static final String LOG_TAG="JSSlidingTabsColors";
    public static final String KEY_PAGE_INDEX="key_page_index";

    /**
     * A custom {@link android.support.v4.view.ViewPager} title strip which looks much like Tabs present in Android v4.0 and
     * above, but is designed to give continuous feedback to the user when scrolling.
     */
    protected SlidingTabLayout mSlidingTabLayout;
    /**
     * viewpager当前选中的位置.
     */
    protected int mPageIndex=0;
    /**
     * A {@link android.support.v4.view.ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
     */
    protected ViewPager mViewPager;
    protected SampleFragmentPagerAdapter mPagerAdapter;

    /**
     * List of {@link cn.archko.abs.demo.fragments.JSSlidingTabsColorsFragment.SamplePagerItem} which represent this sample's tabs.
     */
    protected List<SamplePagerItem> mTabs=new ArrayList<SamplePagerItem>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null!=savedInstanceState&&savedInstanceState.containsKey(KEY_PAGE_INDEX)) {
            mPageIndex=savedInstanceState.getInt(KEY_PAGE_INDEX, 0);
        }
    }

    public void addTab() {
        // BEGIN_INCLUDE (populate_tabs)
        /**
         * Populate our tab list with tabs. Each item contains a title, indicator color and divider
         * color, which are used by {@link SlidingTabLayout}.
         */
        /*mTabs.add(new SamplePagerItem(
            getString(R.string.tab_stream), // Title
            Color.BLUE, // Indicator color
            Color.GRAY // Divider color
        ));

        mTabs.add(new SamplePagerItem(
            getString(R.string.tab_messages), // Title
            Color.RED, // Indicator color
            Color.GRAY // Divider color
        ));

        mTabs.add(new SamplePagerItem(
            getString(R.string.tab_photos), // Title
            Color.YELLOW, // Indicator color
            Color.GRAY // Divider color
        ));

        mTabs.add(new SamplePagerItem(
            getString(R.string.tab_notifications), // Title
            Color.GREEN, // Indicator color
            Color.GRAY // Divider color
        ));*/
        // END_INCLUDE (populate_tabs)
    }

    /**
     * Inflates the {@link android.view.View} which will be displayed by this {@link android.support.v4.app.Fragment}, from the app's
     * resources.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.home_tabs, container, false);

        mViewPager=(ViewPager) view.findViewById(R.id.pager);
        mSlidingTabLayout=(SlidingTabLayout) view.findViewById(R.id.sliding_tabs);

        preSlidingTabLayout();
        addTab();
        postSlidingTabLayout();
        return view;
    }

    protected void preSlidingTabLayout() {
        mSlidingTabLayout.setMatchWidth(true);
        mSlidingTabLayout.setSmoothScroll(false);
        // BEGIN_INCLUDE (tab_colorizer)
        // Set a TabColorizer to customize the indicator and divider colors. Here we just retrieve
        // the tab at the position, and return it's set color
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return mTabs.get(position).getIndicatorColor();
            }

            @Override
            public int getDividerColor(int position) {
                return mTabs.get(position).getDividerColor();
            }

        });
    }

    private void postSlidingTabLayout() {
        mPagerAdapter=new SampleFragmentPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnPageChangeListener(mPagerAdapter);

        if (mPageIndex>0&&mPageIndex<mTabs.size()) {
            mViewPager.setCurrentItem(mPageIndex);
        }
        mSlidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.txt_tab_text);
    }
    // END_INCLUDE (fragment_onviewcreated)

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PAGE_INDEX, mPageIndex);
    }

    public void pageSelected(int i) {

    }

    /**
     * The {@link android.support.v4.app.FragmentPagerAdapter} used to display pages in this sample. The individual pages
     * are instances of {@link ContentFragment} which just display three lines of text. Each page is
     * created by the relevant {@link cn.archko.abs.demo.fragments.JSSlidingTabsColorsFragment.SamplePagerItem} for the requested position.
     * <p>
     * The important section of this class is the {@link #getPageTitle(int)} method which controls
     * what is displayed in the {@link SlidingTabLayout}.
     */
    class SampleFragmentPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

        private final SparseArray<WeakReference<Fragment>> mFragmentArray=new SparseArray<WeakReference<Fragment>>();

        SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Fragment getItem(int position) {
            LLog.v(TAG, "getItem:"+position);
            final WeakReference<Fragment> mWeakFragment=mFragmentArray.get(position);
            if (mWeakFragment!=null&&mWeakFragment.get()!=null) {
                return mWeakFragment.get();
            }

            SamplePagerItem info=mTabs.get(position);
            return Fragment.instantiate(getActivity(), info.clss.getName(), info.args);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            LLog.v(TAG, "instantiateItem:"+position);
            WeakReference<Fragment> mWeakFragment=mFragmentArray.get(position);
            if (mWeakFragment!=null&&mWeakFragment.get()!=null) {
                //mWeakFragment.clear();
                return mWeakFragment.get();
            }

            final Fragment mFragment=(Fragment) super.instantiateItem(container, position);
            mFragmentArray.put(position, new WeakReference<Fragment>(mFragment));
            return mFragment;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void destroyItem(final ViewGroup container, final int position, final Object object) {
            super.destroyItem(container, position, object);
            final WeakReference<Fragment> mWeakFragment=mFragmentArray.get(position);
            if (mWeakFragment!=null) {
                mWeakFragment.clear();
            }
        }

        // BEGIN_INCLUDE (pageradapter_getpagetitle)

        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we return the value returned from {@link cn.archko.abs.demo.fragments.JSSlidingTabsColorsFragment.SamplePagerItem#getTitle()}.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return mTabs.get(position).getTitle();
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            pageSelected(i);
            mPageIndex=i;
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
        // END_INCLUDE (pageradapter_getpagetitle)

    }

}