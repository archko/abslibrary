package cn.archko.abs.demo.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cn.archko.abs.demo.R;
import cn.archko.abs.demo.common.AKHomeViewController;
import cn.archko.abs.demo.common.HomeVolleyHelper;
import cn.archko.abs.demo.model.Dynamic;
import cn.archko.abs.demo.response.ListDynamicResponseData;
import cn.me.archko.abslibrary.common.AKListViewController;
import cn.me.archko.abslibrary.fragment.AbsBaseFragment;
import cn.me.archko.abslibrary.listener.AKDataListener;
import cn.me.archko.abslibrary.widget.AKActionBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;

/**
 * @author: archko 2014/10/23 :17:15
 */
public class HomeFragment extends AbsBaseFragment {

    public static final String TAG="home";
    private ListView mListView;
    private DynamicAdapter mAdapter;
    AKListViewController mListViewController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVolleyHelper=new HomeVolleyHelper();
        mListViewController=new AKHomeViewController();
    }

    @Override
    public View _onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.tab_home, container, false);
        AKActionBar akActionBar=(AKActionBar) root.findViewById(R.id.ak_actionbar);
        akActionBar.setTitle("首页");
        akActionBar.hideBack();
        mListView=(ListView) root.findViewById(R.id.pull_list);
        mAdapter=new DynamicAdapter(getActivity());
        mListView.setAdapter(mAdapter);

        return root;
    }

    @Override
    public void initContainer(View view, LayoutInflater inflater, Bundle savedInstanceState) {
        mListViewController.initContainer(view, inflater, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeVolleyHelper) mVolleyHelper).getListDynamic(getActivity(), false, "", "", mListViewController.getCurrentPage(),
            mListViewController.getPageSize(), TAG,
            new AKDataListener() {
                @Override
                public void onSuccess(Object... args) {
                    if (null!=args&&args.length>0) {
                        ListDynamicResponseData responseData=(ListDynamicResponseData) args[0];
                        if (null!=responseData&&null!=responseData.page&&null!=responseData.page.listDynamic) {
                            mListViewController.setData(responseData);
                            ArrayList<Dynamic> dynamics=responseData.page.listDynamic;
                            if (dynamics.size()<1) {
                                if (isResumed()) {
                                    mListViewController.showDataContainer();
                                }
                            } else {
                                ((DynamicAdapter) mAdapter).setDatas(dynamics);
                                mAdapter.notifyDataSetChanged();
                                if (isResumed()) {
                                    mListViewController.showDataContainer();
                                }
                            }
                            return;
                        }
                        if (isResumed()) {
                            mListViewController.showNoDataContainer(0);
                        }
                    }
                }

                @Override
                public void onFailed(Object... args) {
                    if (isResumed()) {
                        mListViewController.showNoDataContainer(0);
                    }
                }
            });
    }

    static class DynamicAdapter extends BaseAdapter {

        public static final String TAG="CMyCarAdapter";
        Context mContext;
        protected LayoutInflater mInflater;
        ArrayList<Dynamic> mDatas;
        DisplayImageOptions options;

        public DynamicAdapter(Context context) {
            mContext=context;
            mInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mDatas=new ArrayList<Dynamic>();
            options=new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        }

        public void setDatas(ArrayList<Dynamic> datas) {
            this.mDatas=datas;
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CViewHolder viewHolder;

            if (convertView==null) {
                convertView=mInflater.inflate(R.layout.dynamic_item, null);

                viewHolder=new CViewHolder();
                convertView.setTag(viewHolder);
            } else {
                viewHolder=(CViewHolder) convertView.getTag();
            }

            final Dynamic bean=(Dynamic) getItem(position);

            return convertView;
        }

        class CViewHolder {

            public View container;
            public TextView name;
            public TextView txt_car_plate;
            public ImageView img_car_icon;
        }

    }
}
