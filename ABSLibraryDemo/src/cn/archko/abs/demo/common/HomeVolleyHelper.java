package cn.archko.abs.demo.common;

import android.content.Context;
import cn.archko.abs.demo.response.DynamicResponseData;
import cn.archko.abs.demo.response.ListDynamicResponseData;
import cn.archko.abs.demo.response.NewNotifyResponseData;
import cn.archko.abs.demo.utils.Constants;
import cn.me.archko.abslibrary.common.AKAbsVolleyHelper;
import cn.me.archko.abslibrary.listener.AKDataListener;
import cn.me.archko.abslibrary.utils.LLog;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * @author: archko 14/11/9 :21:38
 */
public class HomeVolleyHelper extends AKAbsVolleyHelper {

    private static final String TAG="HomeVolleyHelper";

    //-------------------=======================================
    //方法
    //-------------------=======================================
    @Override
    public Response doParseNetworkResponse(NetworkResponse response, Object tag, Class mJavaClass) {
        try {
            String jsonString=new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            LLog.printResult("parseNetworkResponse:"+tag+"-->"+jsonString);
            Gson mGson=new Gson();
            if ("getNewNotify".equals(tag)) {
                NewNotifyResponseData parsedGSON=mGson.fromJson(jsonString, NewNotifyResponseData.class);
                return Response.success(parsedGSON, HttpHeaderParser.parseCacheHeaders(response));
            } else if ("sendDynamic".equals(tag)||"addLike".equals(tag)||"addComment".equals(tag)) {
                DynamicResponseData parsedGSON=mGson.fromJson(jsonString, DynamicResponseData.class);
                return Response.success(parsedGSON, HttpHeaderParser.parseCacheHeaders(response));
            } else if ("getListDynamic".equals(tag)) {
                ListDynamicResponseData parsedGSON=mGson.fromJson(jsonString, ListDynamicResponseData.class);
                return Response.success(parsedGSON, HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        }
        return super.doParseNetworkResponse(response, tag, mJavaClass);
    }

    /**
     * 获取最新通知数量 getNewNotify
     *
     * @param context
     * @param waiting
     * @param "userID":"用户id",
     * @param "type":"0-普通用户，  1-家长用户，1时该接口有效"
     * @param tag
     * @param dataListener
     */
    public void getNewNotify(Context context, boolean waiting, String userID, String type,
        final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("userID", userID);
            object.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_GETNEWNOTIFY, "getNewNotify", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<NewNotifyResponseData>() {
                @Override
                public void onResponse(NewNotifyResponseData response) {
                    LLog.d(TAG, "notify response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }

    /**
     * 发布动态 sendDynamic
     * 老师用户有效，可发布动态，发布的动态内容包含一组图片和文字
     *
     * @param context
     * @param waiting
     * @param "userID":"用户id",
     * @param "content":"动态内容文本",
     * @param "listImage":["图片对应在up云的url地址",""],
     * @param "listGwClassID":["发送的班级id，如果一个老师带多个班得时候需要让用户选择发送的动态对应班级，可多选，如果无，则全发",""]
     * @param tag
     * @param dataListener
     */
    public void sendDynamic(Context context, boolean waiting, String userID, String content, ArrayList<String> listImage,
        ArrayList<String> listGwClassID, final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("userID", userID);
            object.put("content", content);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_SENDDYNAMIC, "sendDynamic", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<DynamicResponseData>() {
                @Override
                public void onResponse(DynamicResponseData response) {
                    LLog.d(TAG, "notify response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }

    /**
     * 获取动态列表 getListDynamic
     * 根据用户id获取动态列表
     *
     * @param context
     * @param waiting
     * @param "id":"用户id",
     * @param "type":"0-普通用户，1-家长用户",
     * @param "index":                "页码，从0开始",
     * @param "size":                 "每页内容数量，默认20"
     * @param tag
     * @param dataListener
     */
    public void getListDynamic(Context context, boolean waiting, String userId, String type, int index, int size,
        final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("id", userId);
            object.put("type", type);
            object.put("index", index);
            object.put("size", size);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_GETLISTDYNAMIC, "getListDynamic", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<ListDynamicResponseData>() {
                @Override
                public void onResponse(ListDynamicResponseData response) {
                    LLog.d(TAG, "notify response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }

    /**
     * 给动态点赞 addLike
     * dynamic:动态对象
     *
     * @param context
     * @param waiting
     * @param "userId":"点赞的用户id"
     * @param "dynamicID":"被点赞的动态id",
     * @param tag
     * @param dataListener
     */
    public void addLike(Context context, boolean waiting, String userId, String dynamicID,
        final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("dynamicID", dynamicID);
            object.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_ADDLIKE, "addLike", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<DynamicResponseData>() {
                @Override
                public void onResponse(DynamicResponseData response) {
                    LLog.d(TAG, "notify response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }

    /**
     * 给动态添加评论 addComment
     *
     * @param context
     * @param waiting
     * @param "userId":"评论发布人id",
     * @param "dynamicID":"评论的动态id",
     * @param "commentID":"评论针对的评论id，如果该值为空，表示直接对动态进行评论",
     * @param "content":"评论内容"
     * @param tag
     * @param dataListener
     */
    public void addComment(Context context, boolean waiting, String userId, String dynamicID, String commentID,
        String content, final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("dynamicID", dynamicID);
            object.put("userId", userId);
            object.put("commentID", commentID);
            object.put("content", content);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_ADDCOMMENT, "addLike", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<DynamicResponseData>() {
                @Override
                public void onResponse(DynamicResponseData response) {
                    LLog.d(TAG, "notify response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }
}
