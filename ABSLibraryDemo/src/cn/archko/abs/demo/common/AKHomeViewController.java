package cn.archko.abs.demo.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import cn.archko.abs.demo.R;
import cn.archko.abs.demo.response.ListDynamicResponseData;
import cn.archko.abs.demo.view.AKHomeView;
import cn.me.archko.abslibrary.common.AKListViewController;
import cn.me.archko.abslibrary.view.AKEmptyView;
import cn.me.archko.abslibrary.view.AKLoadingView;

/**
 * @author: archko 15/1/12 :16:02
 */
public class AKHomeViewController extends AKListViewController<ListDynamicResponseData> {

    public AKHomeViewController() {
    }

    public void initContainer(View view, LayoutInflater inflater, Bundle savedInstanceState) {
        mWaitingContainer=new AKLoadingView();
        mWaitingContainer.initView(view, inflater);
        mNoDataContainer=new AKEmptyView();
        mNoDataContainer.initView(view, inflater);

        mDataContainer=new AKHomeView();
        mDataContainer.initView(view, inflater);
        mDataContainer.setContentView(view.findViewById(R.id.lay_data_container));
    }

}
