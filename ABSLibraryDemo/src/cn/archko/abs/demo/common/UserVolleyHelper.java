package cn.archko.abs.demo.common;

import android.content.Context;
import android.os.Build;
import cn.archko.abs.demo.BApp;
import cn.archko.abs.demo.response.LoginResponseData;
import cn.archko.abs.demo.utils.AppUtils;
import cn.archko.abs.demo.utils.Constants;
import cn.me.archko.abslibrary.common.AKAbsVolleyHelper;
import cn.me.archko.abslibrary.utils.LLog;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import cn.me.archko.abslibrary.listener.AKDataListener;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * 用户操作类.
 *
 * @author: archko 2014/11/2 :17:34
 */
public class UserVolleyHelper extends AKAbsVolleyHelper {

    private static final String TAG="UserHelper";

    //-------------------=======================================
    //方法
    //-------------------=======================================
    @Override
    public Response doParseNetworkResponse(NetworkResponse response, Object tag, Class mJavaClass) {
        try {
            String jsonString=new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            LLog.printResult("parseNetworkResponse:"+tag+"-->"+jsonString);
            Gson mGson=new Gson();
            if ("login".equals(tag)||"modifyMemberInfo".equals(tag)) {
                LoginResponseData parsedGSON=mGson.fromJson(jsonString, LoginResponseData.class);
                return Response.success(parsedGSON, HttpHeaderParser.parseCacheHeaders(response));
            } else if ("modifyPassword".equals(tag)) {
                LoginResponseData parsedGSON=mGson.fromJson(jsonString, LoginResponseData.class);
                return Response.success(parsedGSON, HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        }
        return super.doParseNetworkResponse(response, tag, mJavaClass);
    }

    public void login(Context context, boolean waiting, String resolution, String channelID, final Object tag,
        final AKDataListener dataListener) {
        String imei=AppUtils.getImei(BApp.getApp());
        String model=AppUtils.getCurrentNetType(BApp.getApp());

        String platform="android";
        String systemName=Build.DISPLAY;
        String systemVersion=Build.VERSION.RELEASE;
        String clientID=imei+"_"+AppUtils.getMacAddress();
        doLogin(context, waiting, clientID, imei, "", platform, model, resolution, systemName, systemVersion, channelID,
            tag, dataListener);
    }

    /**
     * 登陆配置服务 loginConfiguration  登陆配置，用来确定用户的设备身份，如果用户之前登陆过，则将保存的用户名密码输入，这样登陆流程也一起走了。否则仅登记用户的设备信息。
     * Platform: Android
     * Resolution: 480x320, 800x480, 854x480
     * Platform: iOS
     * Model: iPhone1,1 iPhone1,2 iPhone2,1 iPhone3,1 iPhone3,2 iPod1,1 iPod2,1 iPod3,1 iPod4,1 iPad1,1 iPad2,1 iPad2,2 iPad2,3 i386
     * Resolution: 320x480, 640x960, 1024x768
     *
     * @param "clientID":"客户端串号",
     * @param "imei":"手机串号",
     * @param "version":"客户端版本号,四段数字,如1.6.0.0609",
     * @param "platform":"平台,如:iPhone",
     * @param "model":"型号,如:iPhone                 3G",
     * @param "resolution":"分辨率,如:480x320",
     * @param "systemName":"系统名称,如:iOS",
     * @param "systemVersion":"系统版本                (版本号用点号分开)",
     * @param "channelID":"渠道id",
     * @param dataListener
     */
    public void doLogin(Context context, boolean waiting, String clientID, String imei, String version,
        String platform, String model, String resolution, String systemName, String systemVersion, String channelID,
        final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("clientID", clientID);
            object.put("imei", imei);
            object.put("version", version);
            object.put("platform", platform);
            object.put("model", model);
            object.put("resolution", resolution);
            object.put("systemName", systemName);
            object.put("systemVersion", systemVersion);
            object.put("channelID", channelID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_LOGIN, "login", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<LoginResponseData>() {
                @Override
                public void onResponse(LoginResponseData response) {
                    LLog.d(TAG, "login response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }

    /**
     * 修改个人会员信息 modifyMemberInfo
     *
     * @param context
     * @param waiting
     * @param "nick":"用户昵称",
     * @param "image":"头像的图片地址"
     * @param tag
     * @param dataListener
     */
    public void modifyMemberInfo(Context context, boolean waiting, String nick, String image,
        final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("nick", nick);
            object.put("image", image);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_MODIFYMEMBERINFO, "modifyMemberInfo", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<LoginResponseData>() {
                @Override
                public void onResponse(LoginResponseData response) {
                    LLog.d(TAG, "login response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }

    /**
     * 修改密码 modifyPassword
     *
     * @param context
     * @param waiting
     * @param "oldPassword":"老密码，md5加密",
     * @param "newPassword":"新密码，md5加密"
     * @param tag
     * @param dataListener
     */
    public void modifyPassword(Context context, boolean waiting, String oldPassword, String newPassword,
        final Object tag, final AKDataListener dataListener) {
        JSONObject object;
        object=new JSONObject();
        try {
            object.put("oldPassword", oldPassword);
            object.put("newPassword", newPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (waiting) {
            showDialog(context);
        }
        sendRequest(Constants.JS_HTTP_SCHEMA+Constants.JS_METHOD_MODIFYPASSWORD, "modifyPassword", "Ustinput="+object.toString(), dataListener,
            new Response.Listener<LoginResponseData>() {
                @Override
                public void onResponse(LoginResponseData response) {
                    LLog.d(TAG, "login response:"+response);
                    dismissDialog();

                    if (null!=dataListener) {
                        dataListener.onSuccess(response, tag);
                    }
                }
            });
    }
}