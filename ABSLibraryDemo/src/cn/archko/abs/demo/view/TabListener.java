package cn.archko.abs.demo.view;

/**
 * @author: archko 2014/4/9 :10:24
 */
public interface TabListener {

    public int getSelectedIdx();
}
