package cn.archko.abs.demo.view;

/**
 * @version 1.00.00
 * @description:
 * @author: archko 11-12-17
 */
public interface IBaseItemView {

    void update(final Object bean, boolean updateFlag);
}
