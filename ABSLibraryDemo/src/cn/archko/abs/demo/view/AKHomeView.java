package cn.archko.abs.demo.view;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import cn.archko.abs.demo.R;
import cn.me.archko.abslibrary.view.AKView;

/**
 * 首页视图.
 *
 * @author: archko 14/11/18 :15:30
 */
public class AKHomeView extends AKView {

    ListView mListView;

    @Override
    public View initView(View view, LayoutInflater inflater) {
        mContainer=view.findViewById(R.id.lay_data_container);
        mListView=(ListView) view.findViewById(R.id.pull_list);
        return mContainer;
    }
}
