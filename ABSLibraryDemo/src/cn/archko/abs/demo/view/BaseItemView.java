package cn.archko.abs.demo.view;

import android.content.Context;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 作为基础类，可以直接使用，但没有头像点击功能。
 *
 * @author: archko 12-6-24
 */
public abstract class BaseItemView extends LinearLayout implements
    IBaseItemView, View.OnClickListener, View.OnTouchListener {

    private static final String TAG="BaseItemView";
    protected Context mContext;

    public BaseItemView(Context context) {
        super(context);
        mContext=context;
    }

    /**
     * 更新内容
     *
     * @param bean       实体
     * @param updateFlag 更新标志，如果为true表示更新图片
     */
    public void update(Object bean, boolean updateFlag) {
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        boolean ret=false;
        CharSequence text=((TextView) v).getText();
        Spannable stext=Spannable.Factory.getInstance().newSpannable(text);
        TextView widget=(TextView) v;
        int action=event.getAction();

        if (action==MotionEvent.ACTION_UP||
            action==MotionEvent.ACTION_DOWN) {
            int x=(int) event.getX();
            int y=(int) event.getY();

            x-=widget.getTotalPaddingLeft();
            y-=widget.getTotalPaddingTop();

            x+=widget.getScrollX();
            y+=widget.getScrollY();

            Layout layout=widget.getLayout();
            int line=layout.getLineForVertical(y);
            int off=layout.getOffsetForHorizontal(line, x);

            ClickableSpan[] link=stext.getSpans(off, off, ClickableSpan.class);
            //Log.d(TAG, "onTouch:"+link.length+" text:");
            if (link.length!=0) {
                int start=stext.getSpanStart(link[0]);
                int end=stext.getSpanEnd(link[0]);
                //Log.d(TAG, "onTouch start:"+start+" end:"+end);
                if (action==MotionEvent.ACTION_UP) {
                    link[0].onClick(widget);
                    Selection.removeSelection(stext);
                } else if (action==MotionEvent.ACTION_DOWN) {
                    Selection.setSelection(stext, start, end);
                }
                ret=true;
            } else {
                Selection.removeSelection(stext);
            }
            //Log.d(TAG, "onTouch:ret:"+ret);
        }
        return ret;
    }
}