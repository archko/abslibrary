package cn.archko.abs.demo.model;

/**
 * 班级对象，包含班级的入学时间，班级序号，班级昵称，当前用户是否班主任
 * @author: archko 14/11/3 :21:19
 */
public class GwClass {

    public String type;//":"0-老师用户、1-家长用户",
    public String isCharge;//":"是否班主任，为老师用户时有效",
    public String isGeneral;//":"true-普通班级，false-兴趣班",
    public String joinAge;//":"入学年份，为2001,2010这种年份数据（4位）",
    public String semesterType;//":"0-秋季学期、1-春季学期",
    public String name;//":"班级昵称，如小一班、舞蹈班等"
}
