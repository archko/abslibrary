package cn.archko.abs.demo.model;

/**
 * 通知消息对象 notification
 *
 * @author: archko 14/11/3 :21:25
 */
public class Notification {

    public String notifCode;//":"通知信息代号",
    public String notifInfo;//":"通知信息描述"

    public String getNotifCode() {
        return notifCode;
    }

    public void setNotifCode(String notifCode) {
        this.notifCode=notifCode;
    }

    public String getNotifInfo() {
        return notifInfo;
    }

    public void setNotifInfo(String notifInfo) {
        this.notifInfo=notifInfo;
    }

    @Override
    public String toString() {
        return "Notification{"+
            "notifCode='"+notifCode+'\''+
            ", notifInfo='"+notifInfo+'\''+
            '}';
    }
}
