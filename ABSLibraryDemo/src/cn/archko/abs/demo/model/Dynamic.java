package cn.archko.abs.demo.model;

import java.util.ArrayList;

/**
 * 老师发送的关于学生活动的图片和相关文本，类似微信朋友圈
 * sendGwMember 发送者用户对象
 * listImage 图片列表
 *
 * @author: archko 14/11/3 :21:20
 */
public class Dynamic {

    public String id;//":"动态id",
    public User sendGwMember;//":{},
    public String content;//":"动态的文本内容",
    public ArrayList<String> listImage;//":[],
    public ArrayList<Like> listLike;//":[],
    public ArrayList<Comment> listComment;//":[],
    public String sendTime;//":"动态发布时间， 格式为 YYYY-MM-DD HH:MM:SS"

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id=id;
    }

    public User getSendGwMember() {
        return sendGwMember;
    }

    public void setSendGwMember(User sendGwMember) {
        this.sendGwMember=sendGwMember;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content=content;
    }

    public ArrayList<String> getListImage() {
        return listImage;
    }

    public void setListImage(ArrayList<String> listImage) {
        this.listImage=listImage;
    }

    public ArrayList<Like> getListLike() {
        return listLike;
    }

    public void setListLike(ArrayList<Like> listLike) {
        this.listLike=listLike;
    }

    public ArrayList<Comment> getListComment() {
        return listComment;
    }

    public void setListComment(ArrayList<Comment> listComment) {
        this.listComment=listComment;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime=sendTime;
    }

    @Override
    public String toString() {
        return "Dynamic{"+
            "id='"+id+'\''+
            ", sendGwMember="+sendGwMember+
            ", content='"+content+'\''+
            ", listImage="+listImage+
            ", listLike="+listLike+
            ", listComment="+listComment+
            ", sendTime='"+sendTime+'\''+
            '}';
    }
}
