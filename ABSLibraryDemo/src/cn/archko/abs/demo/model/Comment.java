package cn.archko.abs.demo.model;

/**
 * 用来描述一个评论
 *
 * @author: archko 14/11/3 :21:22
 */
public class Comment {

    public String dynamicID;//":"评论针对的动态id",
    public String id;//":"评论id",
    public String destCommentID;//":"评论针对的评论id，如果为空，表示直接对动态进行评论",
    public String userId;//":"评论发布人id",
    public String content;//":"评论内容",
    public String nick;//":"评论发布人昵称"
}
