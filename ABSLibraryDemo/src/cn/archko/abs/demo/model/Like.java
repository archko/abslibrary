package cn.archko.abs.demo.model;

/**
 * 用户点赞对象，赞的实体内容包含点赞的用户id、用户nick、用户头像、用户类型
 *
 * @author: archko 14/11/3 :21:21
 */
public class Like {

    public String userId;//":"赞的发布人的id",
    public String type;//":"用户类型，0-普通用户、1-家长用户",
    public String nick;//":"用户昵称",
    public String image;//":"用户头像"
}
