package cn.archko.abs.demo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * @author: archko 14-2-28 :下午2:23
 */
public class SLImageBean implements Parcelable, Serializable {

    private static final long serialVersionUID=5979202924337286603L;
    public int id;
    public String url;
    public String thumb;
    public String name;
    public String desc;
    public String path;

    public static final Parcelable.Creator<SLImageBean> CREATOR=new Parcelable.Creator<SLImageBean>() {
        @Override
        public SLImageBean createFromParcel(Parcel source) {
            return new SLImageBean(source);
        }

        @Override
        public SLImageBean[] newArray(int size) {
            return new SLImageBean[size];
        }
    };

    public SLImageBean() {
    }

    public SLImageBean(Parcel in) {
        readFromParcel(in);
    }

    protected void readFromParcel(Parcel in) {
        id=in.readInt();
        url=in.readString();
        thumb=in.readString();
        name=in.readString();
        desc=in.readString();
        path=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(url);
        dest.writeString(thumb);
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeString(path);
    }

    @Override
    public String toString() {
        return "SLImageBean{"+
            "id="+id+
            ", url='"+url+'\''+
            ", thumb='"+thumb+'\''+
            ", name='"+name+'\''+
            ", desc='"+desc+'\''+
            ", path='"+path+'\''+
            '}';
    }
}
