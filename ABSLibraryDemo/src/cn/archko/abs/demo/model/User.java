package cn.archko.abs.demo.model;

import cn.archko.abs.demo.model.GwClass;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author: archko 2014/10/9 :10:27
 */
public class User implements Serializable {

    public static final long serialVersionUID=3894560643019408213L;

    public long _id;
    public String userId;
    public String name;
    public String pass;
    public String token;
    public String secret;
    //==========
    public String id;//": "用户ID",
    public String type;//":"普通的代理商/教师用户或者家长用户，0-普通用户；1-家长用户，和id一起决定一个用户",
    public String mobile;//":"手机号码，对于普通用户，全网唯一；对于家长用户，跟密码一起全网唯一",
    public String role;//":"普通用户有效，0-幼儿园管理员，1-老师（包含班主任和普通老师）",
    public String nick;//":"昵称，家长用户默认为 学生名字+爸爸、妈妈这种",
    public String image;//":"头像，可以在手机端修改,后台设置默认头像",
    public ArrayList<GwClass> listGwClass;//":[]

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id=_id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId=userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass=pass;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token=token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret=secret;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id=id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type=type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile=mobile;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role=role;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick=nick;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image=image;
    }

    public ArrayList<GwClass> getListGwClass() {
        return listGwClass;
    }

    public void setListGwClass(ArrayList<GwClass> listGwClass) {
        this.listGwClass=listGwClass;
    }

    @Override
    public String toString() {
        return "User{"+
            "_id="+_id+
            ", userId='"+userId+'\''+
            ", name='"+name+'\''+
            ", pass='"+pass+'\''+
            ", token='"+token+'\''+
            ", secret='"+secret+'\''+
            ", id='"+id+'\''+
            ", type='"+type+'\''+
            ", mobile='"+mobile+'\''+
            ", role='"+role+'\''+
            ", nick='"+nick+'\''+
            ", image='"+image+'\''+
            ", listGwClass="+listGwClass+
            '}';
    }
}
