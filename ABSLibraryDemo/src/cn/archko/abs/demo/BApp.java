package cn.archko.abs.demo;

import cn.me.archko.abslibrary.AKAbsApp;

/**
 * @author: archko 14-10-7 :下午1:46
 */
public class BApp extends AKAbsApp {

    public static BApp getApp() {
        return (BApp) sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance=this;
    }
}
