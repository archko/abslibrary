package cn.archko.abs.demo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import cn.archko.abs.demo.BApp;
import cn.me.archko.abslibrary.utils.LLog;

import java.io.File;

/**
 * @author: archko 2014/10/11 :10:19
 */
public class AppUtils {

    /**
     * 获取上传的目录.
     *
     * @param dir 目录名,在用户上传时,以用户id为目录名.为每一个用户建立一个目录.
     * @return
     */
    public static String getUploadPath(String dir) {
        if (TextUtils.isEmpty(dir)) {
            return null;
        }
        String SAVE_KEY=File.separator+dir+File.separator+System.currentTimeMillis();
        return SAVE_KEY;
    }

    /**
     * 获取imei号，如果没有，需要获取唯一的号码。
     *
     * @param context
     * @return
     */
    public static String getImei(Context context) {
        TelephonyManager telephonyManager=(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager==null) {
            LLog.w("", "No IMEI.");
        }

        String str="";
        try {
            //if (a(paramContext, "android.permission.READ_PHONE_STATE")){
            str=telephonyManager.getDeviceId();
        } catch (Exception localException) {
            LLog.w("", "No IMEI."+localException);
        }

        if (TextUtils.isEmpty(str)) {
            LLog.w("", "No IMEI.");
            str=getMacAddress();
            if (TextUtils.isEmpty(str)) {
                LLog.w("", "Failed to take mac as IMEI. Try to use Secure.ANDROID_ID instead.");
                str=Settings.Secure.getString(context.getContentResolver(), "android_id");
                LLog.w("", "getDeviceId: Secure.ANDROID_ID: "+str);
                return str;
            }
        }

        return str;
    }

    /**
     * 获取mac地址
     *
     * @return
     */
    public static String getMacAddress() {
        String macAddress="";
        WifiManager wifiMgr=(WifiManager) BApp.getApp().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info=(null==wifiMgr ? null : wifiMgr.getConnectionInfo());
        if (null!=info) {
            macAddress=info.getMacAddress();
            if (!TextUtils.isEmpty(macAddress)) {
                macAddress=macAddress.replaceAll(":", "");
            }
        }

        return macAddress;
    }
    /**
     * 获取网络类型
     *
     * @param mContext
     * @return
     */
    public static String getCurrentNetType(Context mContext) {
        ConnectivityManager connManager=(ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni=connManager.getActiveNetworkInfo();
        if (null==ni) {
            return "other";
        }

        int networkType=ni.getType();
        int subType=ni.getSubtype();

        if (networkType==ConnectivityManager.TYPE_WIFI) {
            return "wifi";
        } else if (networkType==ConnectivityManager.TYPE_MOBILE) {
            // 3G (or better)
            if (subType>=TelephonyManager.NETWORK_TYPE_UMTS) {
                return "3g";
            }

            // GPRS (or unknown)
            if (subType==TelephonyManager.NETWORK_TYPE_GPRS
                ||subType==TelephonyManager.NETWORK_TYPE_UNKNOWN) {
                return "gprs";
            }

            // EDGE
            if (subType==TelephonyManager.NETWORK_TYPE_EDGE) {
                return "edge";
            }
        }

        return "other";
    }
}
