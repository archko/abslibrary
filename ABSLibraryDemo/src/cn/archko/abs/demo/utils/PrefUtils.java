package cn.archko.abs.demo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import cn.archko.abs.demo.BApp;

/**
 * 配置相关的工具类.
 *
 * @author: archko 2014/10/11 :10:09
 */
public class PrefUtils {

    //-------------------=======================================

    public static final SharedPreferences getPrefs(String name) {
        return BApp.getApp().getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static final SharedPreferences getDefaultPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(BApp.getApp());
    }

    //-------------------=======================================

    //---------------------------- prefs file ----------------------------

    /**
     * 存储用户信息的文件
     */
    public static final String PREFS_FILE_ACCOUNT="prefs_file_account.jso";

    //---------------------------- prefs key ----------------------------
    /**
     * 是否自动登录的key
     */
    public static final String PREFS_KEY_AUTO_LOGIN="prefs_key_auto_login";

    /**
     * 登录用的,包含了用户名与密码组合而成的.
     */
    public static final String PREFS_KEY_LOGIN_STRING="prefs_key_login_string";
}
