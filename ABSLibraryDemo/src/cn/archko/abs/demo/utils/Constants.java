package cn.archko.abs.demo.utils;

/**
 * @author: archko 2014/9/4 :20:08
 */
public class Constants {

    public static final String KEY="abcdefgopqrstuvwxyzhijklmn";

    public static String[] PING_URLS={
        "www.baidu.com",
        "www.qq.com",
        "www.taobao.com"};

    //-------------------=======================================
    //url与方法名
    //-------------------=======================================
    public static final String JS_HTTP_SCHEMA="http://";
    /**
     *
     */
    public static final String JS_METHOD_LOGIN="loginConfiguration";
    public static final String JS_METHOD_MODIFYMEMBERINFO="modifyMemberInfo";
    public static final String JS_METHOD_MODIFYPASSWORD="modifyPassword";
    public static final String JS_METHOD_GETNEWNOTIFY="getNewNotify";
    public static final String JS_METHOD_SENDDYNAMIC="sendDynamic";
    public static final String JS_METHOD_GETLISTDYNAMIC="getListDynamic";
    public static final String JS_METHOD_ADDLIKE="addLike";
    public static final String JS_METHOD_ADDCOMMENT="addComment";
    //public static final String JS_USER_AGENT="Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1797.2 Safari/537.36";

    //-------------------=======================================
    //存储的文件名.放在/data/data/packagename/files/下,可以序列化,也可以使用工具导出json文本.
    //-------------------=======================================
    /**
     * 用户信息存储的文件名.
     */
    public static final String JS_FILE_USER="js_file_user.jso";
    /**
     * 存储消息文件名.
     */
    public static final String JS_FILE_MESSAGE="js_file_message.jso";

    //-------------------=======================================
    //内存的key
    //-------------------=======================================
    /**
     * 用户存储的key,登录后,存储在内存中.
     */
    public static final String JS_KEY_USER="js_key_user";
}
