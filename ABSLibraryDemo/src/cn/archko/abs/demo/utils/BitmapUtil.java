package cn.archko.abs.demo.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * @author: archko 14/11/18 :15:50
 */
public class BitmapUtil {

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height=options.outHeight;
        final int width=options.outWidth;
        int inSampleSize=1;

        if (height>reqHeight||width>reqWidth) {
            if (width>height) {
                inSampleSize=Math.round((float) height/(float) reqHeight);
            } else {
                inSampleSize=Math.round((float) width/(float) reqWidth);
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String filepath, int reqWidth, int reqHeight) {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options=new BitmapFactory.Options();
            options.inJustDecodeBounds=true;
            BitmapFactory.decodeFile(filepath, options);

            // Calculate inSampleSize
            options.inSampleSize=calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds=false;
            options.inDither=false;
            options.inPreferredConfig=Bitmap.Config.RGB_565;
            return BitmapFactory.decodeFile(filepath, options);
        } catch (OutOfMemoryError error) {
            System.gc();
        } catch (Exception e) {
        }
        return null;
    }

}
