package cn.archko.abs.demo.activities;

import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.archko.abs.demo.BApp;
import cn.archko.abs.demo.R;
import cn.archko.abs.demo.model.SLImageBean;
import cn.archko.abs.demo.view.HackyViewPager;
import cn.me.archko.abslibrary.activities.AKActivity;
import cn.me.archko.abslibrary.utils.AKNotifyUtil;
import cn.me.archko.abslibrary.utils.LLog;
import cn.me.archko.abslibrary.widget.AKActionBar;

import java.util.ArrayList;
//import com.ganji.android.jujiabibei.viewpagerindicator.PageIndicator;

/**
 * @description:
 * @author: archko 13-9-22 :上午10:27
 */
public class ImageViewerActivity extends AKActivity {

    public static final String TAG="ImageViewerActivity";
    private HackyViewPager mViewPager;
    //SamplePagerAdapter mPagerAdapter;
    ArrayList<SLImageBean> mSLImageList;
    int mSelectedIdx;
    ImageView mSave;
    //PageIndicator indicator;
    TextView mPager;
    protected AKActionBar mSlActionBar;
    TextView mImageInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().hide();

        if (null==getIntent()||null==getIntent().getExtras()) {
            LLog.e(TAG, "null==getIntent");
            AKNotifyUtil.showToast(BApp.getApp(), "null==getIntent");
            finish();
            return;
        }
        String title="";
        title=getIntent().getStringExtra("title");
        LLog.d(TAG, "title:"+title);
        mSLImageList=getIntent().getExtras().getParcelableArrayList("thumbs");
        mSelectedIdx=getIntent().getIntExtra("pos", 0);
        LLog.d(TAG, "mSelectedIdx:"+mSelectedIdx);

        if (null==mSLImageList||mSLImageList.size()<1) {
            LLog.e(TAG, "null==url");
            AKNotifyUtil.showToast(BApp.getApp(), "系统错误,没有图片可以查看.");
            finish();
            return;
        }

        setContentView(R.layout.imageviewer);
        mSlActionBar=(AKActionBar) findViewById(R.id.sl_actionbar);
        //mSlActionBar.setBackground(R.drawable.sl_bg_bar_title);
        mSlActionBar.setBackImage(0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /*mSlActionBar.setBackground(R.drawable.sl_bg_bar_title);
        mSlActionBar.getTxtTitle().setTextColor(getResources().getColor(R.color.sl_white));
        mSlActionBar.getTxtSubTitle().setTextColor(getResources().getColor(R.color.sl_white));*/
        mImageInfo=(TextView) findViewById(R.id.txt_imageinfo);
        if (!TextUtils.isEmpty(title)) {
            mSlActionBar.setTitle(title);
        }

        mViewPager=(HackyViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(0);
        mViewPager.setPageMargin(20);
        mSave=(ImageView) findViewById(R.id.save);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });
        //indicator=(PageIndicator) findViewById(R.id.indicator);
        mPager=(TextView) findViewById(R.id.txt_pager);

        /*mPagerAdapter=new SamplePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        //mViewPager.setCurrentItem(mSelectedIdx);
        mViewPager.setOnPageChangeListener(mPagerAdapter);

        if (mSelectedIdx!=0) {
            mViewPager.setCurrentItem(mSelectedIdx);
        }
        mViewPager.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                LLog.d(TAG, "onPreDraw:"+mSelectedIdx);
                mViewPager.getViewTreeObserver().removeOnPreDrawListener(this);
                mPagerAdapter.onPageSelected(mSelectedIdx);
                return true;
            }
        });*/
        showOverlay();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void saveImage() {

    }

    /*class SamplePagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

        private final SparseArray<WeakReference<SLImageView>> mFragmentArray=new SparseArray<WeakReference<SLImageView>>();

        public SamplePagerAdapter(FragmentManager fm) {
            super();
        }

        @Override
        public int getCount() {
            return mSLImageList.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            SLImageView itemView=null;
            SLImageBean bean=mSLImageList.get(position);
            final WeakReference<SLImageView> mWeakFragment=mFragmentArray.get(position);
            if (mWeakFragment!=null&&mWeakFragment.get()!=null) {
                itemView=mWeakFragment.get();
                itemView.update(bean);
                container.addView(itemView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                return itemView;
            }

            itemView=new SLImageView(ImageViewerActivity.this, bean);
            itemView.update(bean);
            //itemView.loadThumb();
            mFragmentArray.put(position, new WeakReference<SLImageView>(itemView));
            container.addView(itemView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
            final WeakReference<SLImageView> mWeakFragment=mFragmentArray.get(position);
            if (mWeakFragment!=null) {
                if (mWeakFragment.get()!=null) {
                    SLImageView itemView=mWeakFragment.get();
                    if (null!=itemView) {
                        itemView.stopDownload();
                    }
                }
                mWeakFragment.clear();
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        public SLImageView getImageView(int position) {
            SLImageView itemView=null;
            final WeakReference<SLImageView> mWeakFragment=mFragmentArray.get(position);
            if (mWeakFragment!=null&&mWeakFragment.get()!=null) {
                itemView=mWeakFragment.get();
            }
            return itemView;
        }

        @Override
        public void onPageScrolled(int i, float v, int i2) {
        }

        @Override
        public void onPageSelected(int i) {
            mSelectedIdx=i;
            SLImageBean bean=mSLImageList.get(i);
            LLog.d(TAG, "onPageSelected."+i+" bean:"+bean);
            if (null!=bean) {
                mImageInfo.setText(bean.desc);
            }
            updatePager();
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }*/

    private void updatePager() {
        mPager.setText((mSelectedIdx+1)+"/"+mSLImageList.size());
    }

    public class SaveImageTask extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            boolean flag=false;
            String targetFilePath=null;
            try {
                String path=(String) params[0];
                //flag=ImageCache.getInstance().getImageManager().copyFileToFile(targetFilePath, path);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return flag ? targetFilePath : null;
        }

        protected void onPostExecute(String bitmap) {
            if (!isFinishing()) {
                if (bitmap!=null) {
                    AKNotifyUtil.showToast(BApp.getApp(), "保存成功:"+bitmap, Toast.LENGTH_LONG);
                } else {
                    AKNotifyUtil.showToast(BApp.getApp(), "保存失败:");
                }
            }
        }
    }

    //--------------------------------------------------

    private boolean mShowing=false;
    private static final int OVERLAY_TIMEOUT=3000;
    private static final int FADE_OUT=1;
    private final Handler mHandler=new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FADE_OUT:
                    hideOverlay(false);
                    break;
            }
        }
    };

    /**
     * show overlay the the default timeout
     */
    private void showOverlay() {
        showOverlay(OVERLAY_TIMEOUT);
    }

    /**
     * show overlay
     */
    private void showOverlay(int timeout) {
        LLog.d(TAG, "showOverlay:"+timeout);
        if (!mShowing) {
            mShowing=true;
            mSlActionBar.setVisibility(View.VISIBLE);
            mImageInfo.setVisibility(View.VISIBLE);
        }
        /*Message msg=mHandler.obtainMessage(FADE_OUT);
        if (timeout!=0) {
            mHandler.removeMessages(FADE_OUT);
            mHandler.sendMessageDelayed(msg, timeout);
        }*/
    }

    /**
     * hider overlay
     */
    private void hideOverlay(boolean fromUser) {
        LLog.d(TAG, "hideOverlay:"+fromUser);
        if (mShowing) {
            if (!fromUser) {
                mSlActionBar.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
                mImageInfo.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
            }
            mSlActionBar.setVisibility(View.INVISIBLE);
            mImageInfo.setVisibility(View.INVISIBLE);
            mShowing=false;
        }
    }

    public void toggle() {
        if (!mShowing) {
            showOverlay();
        } else {
            hideOverlay(true);
        }
    }
}
