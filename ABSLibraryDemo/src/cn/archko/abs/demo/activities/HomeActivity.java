package cn.archko.abs.demo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import cn.archko.abs.demo.BApp;
import cn.archko.abs.demo.R;
import cn.archko.abs.demo.fragments.HomeTabFragment;
import cn.me.archko.abslibrary.activities.AKActivity;
import cn.me.archko.abslibrary.utils.AKNotifyUtil;
import cn.me.archko.abslibrary.utils.LLog;

/**
 * @author: archko 13-12-16 :下午2:01
 */
public class HomeActivity extends AKActivity {

    public static final String TAG="home";
    public static final int REQUEST_FEATURE_CODE=0X010;
    Handler mHandler=new Handler();
    View mSplash;
    TextView mBtn;
    public static final int REQUEST_REFRESH_ORDERLIST_CODE=0x012;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        mSplash=findViewById(R.id.img_splash);
        mBtn=(TextView) findViewById(R.id.btn_tip);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    void init() {
        long newTime=0l;
        /*if (newTime<2000l) {
            newTime=2000l-newTime;
        } else {
            newTime=0;
        }*/

        LLog.d(TAG, "newTime:"+newTime);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                initFragment();
                mSplash.setVisibility(View.GONE);
                mSplash.setBackgroundDrawable(null);

                if (null!=getIntent().getExtras()) {
                    int current_item=getIntent().getExtras().getInt("current_item", 0);
                    LLog.d(TAG, "oncreate current_item:"+current_item+" extras:"+getIntent().getExtras());
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            processIntent(getIntent());
                        }
                    }, 1000l);
                }
            }
        }, newTime);

    }

    public void initFragment() {
        Intent intent=getIntent();
        if (null==intent) {
            finish();
            return;
        }

        String title=intent.getStringExtra("title");
        String className=HomeTabFragment.class.getName();
        //String className=intent.getStringExtra("fragment_class");

        try {
            Fragment old=getSupportFragmentManager().findFragmentById(R.id.content);
            LLog.d(TAG, "initFragment."+className+" old:"+old);
            if (null==old) {
                Fragment newFragment=Fragment.instantiate(this, className, intent.getExtras());
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                ft.add(R.id.content, newFragment).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    public void processIntent(Intent intent) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LLog.d(TAG, "ActivityResult:"+requestCode+" resultCode:"+resultCode+" data:"+data);
        if (requestCode==REQUEST_FEATURE_CODE) {
            initFragment();
            mSplash.setVisibility(View.GONE);
            mSplash.setBackgroundDrawable(null);
        } else if (REQUEST_REFRESH_ORDERLIST_CODE==requestCode) {
            Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.content);
            if (fragment!=null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    //-----------------------------------------------
    private static final int RELEASE_EXIT_CHECK_TIMEOUT=3500;
    private boolean mExitFlag=false;
    private long mExitBackTimeout=-1;

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        /*if (keyCode==KeyEvent.KEYCODE_BACK) {
            if (checkBackAction()) {
                return true;
            }

            // An exit event has occurred, force the destroy the consoles
            finish();
        }*/
        return super.onKeyUp(keyCode, event);
    }

    private boolean checkBackAction() {
        //Do back operation over the navigation history
        boolean flag=this.mExitFlag;

        this.mExitFlag=!false;

        // Retrieve if the exit status timeout has expired
        long now=System.currentTimeMillis();
        boolean timeout=(this.mExitBackTimeout==-1||
            (now-this.mExitBackTimeout)>RELEASE_EXIT_CHECK_TIMEOUT);

        //Check if there no history and if the user was advised in the last back action
        if (this.mExitFlag&&(this.mExitFlag!=flag||timeout)) {
            //Communicate the user that the next time the application will be closed
            this.mExitBackTimeout=System.currentTimeMillis();
            AKNotifyUtil.showToast(BApp.getApp(), "再次点击即可退出.");
            return true;
        }

        //Back action not applied
        return !this.mExitFlag;
    }

    //-----------------------------------------------

    /**
     * 显示遮罩层
     *
     * @param bgListener  背景点击监听器,默认为隐藏
     * @param btnListener 按钮的监听器,可为空
     * @param resId       背景资源为drawable
     */
    public void showTip(View.OnClickListener bgListener, View.OnClickListener btnListener, int resId, int txtId) {
        mSplash.setVisibility(View.VISIBLE);
        mSplash.setBackgroundResource(resId);
        if (null==bgListener) {
            if (txtId!=0) {     //认为有按钮.
                mSplash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSplash.setBackgroundDrawable(null);
                        mSplash.setVisibility(View.GONE);
                        mSplash.setOnClickListener(null);
                    }
                });
            }
        } else {
            mSplash.setOnClickListener(bgListener);
        }
        mBtn.setOnClickListener(btnListener);
        if (txtId!=0) {
            mBtn.setText(txtId);
            mBtn.setVisibility(View.VISIBLE);
        } else {
            mBtn.setVisibility(View.GONE);
        }
    }

    /**
     * 隐藏遮罩层
     */
    public void hideTip() {
        if (mSplash.getVisibility()==View.VISIBLE) {
            mSplash.setVisibility(View.GONE);
            mSplash.setBackgroundDrawable(null);
            mSplash.setOnClickListener(null);
            mBtn.setOnClickListener(null);
        }
    }
}
